# -*- coding: utf-8 -*-
# Copyright (C) 2025 Cresset Biomolecular Discovery Ltd.
# Released under CC-BY-SA 4.0 (https://creativecommons.org/licenses/by-sa/4.0/).
# Originally downloaded from https://gitlab.com/cresset
"""Adds a 'Create Ligand From HELM' menu item to Flare.

Ribbon Controls:
    Ligand -> New -> Create Ligand From HELM
        Open a dialog to input a HELM string.
        After pressing 'Ok', the ligand is added to the Ligands Table.
"""
from cresset import flare
from PySide2.QtWidgets import (
    QDialog,
    QDialogButtonBox,
    QLabel,
    QMessageBox,
    QPlainTextEdit,
    QVBoxLayout,
)
from rdkit.Chem.rdchem import Mol

from helm.parser import HelmParser, InvalidHelmError


@flare.extension
class HELMNewExtension:
    """Add menu item to the New ligand button."""

    def load(self):
        ribbon_control = flare.main_window().ribbon["Ligand"]["Edit"]["New"]
        menu = ribbon_control.menu()
        action = menu.addAction("Create Ligand From HELM", self.main)
        action.setStatusTip("Create a new ligand from a HELM string.")
        ribbon_control.set_menu_action_tip_key(action, "H")
        print(f"Loaded {self.__class__.__module__}.{self.__class__.__name__}")

    def main(self):
        helm_dialog = HELMDialog(parent=flare.main_window().widget())
        helm_dialog.exec()


class HELMDialog(QDialog):
    """Dialog containing a large text area to input a HELM string."""

    def __init__(self, parent=None):
        """Create the dialog."""
        super().__init__(parent=parent)

        self.setWindowTitle("Create Molecule")

        self.edit = QPlainTextEdit()
        self.edit.resize(600, 300)

        button_box = QDialogButtonBox(QDialogButtonBox.Ok | QDialogButtonBox.Cancel)
        button_box.accepted.connect(self._parse_text)
        button_box.rejected.connect(self.reject)

        layout = QVBoxLayout(self)
        layout.addWidget(QLabel("Input HELM:"))
        layout.addWidget(self.edit)
        layout.addWidget(button_box)

    def _parse_text(self):
        """Parse the text and create a Ligand."""
        parser = HelmParser()

        try:
            mol = parser.parse(self._helm_string())
        except InvalidHelmError as e:
            return QMessageBox.critical(self, "Invalid HELM", f"Invalid HELM: {e}")

        self._add_molecule_to_project(mol)
        self.accept()

    def _add_molecule_to_project(self, mol: Mol):
        """Add the new ligand to the project."""
        project = flare.main_window().project
        try:
            role = project.roles[-1]
            ligand = role.append(mol)
        except Exception:
            ligand = project.ligands.append(mol)
        ligand.title = self._helm_string()
        flare.main_window().create_undo_point("Create Ligand From HELM")

    def _helm_string(self) -> str:
        """Get the string from the widget and format appropriately."""
        text = self.edit.toPlainText()
        return "".join([line.strip() for line in text.split("\n")])
