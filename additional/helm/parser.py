# -*- coding: utf-8 -*-
# Copyright (C) 2025 Cresset Biomolecular Discovery Ltd.
# Released under CC-BY-SA 4.0 (https://creativecommons.org/licenses/by-sa/4.0/).
# Originally downloaded from https://gitlab.com/cresset
import gzip
import json
import pickle
import re
import os
import io
from pathlib import Path
from typing import Optional, Tuple, Callable, List
from enum import Enum

from rdkit import Chem
from rdkit.Chem.rdDepictor import Compute2DCoords, SetPreferCoordGen
from rdkit.Chem.rdchem import Mol, Atom

SetPreferCoordGen(True)


class InvalidHelmError(Exception):
    """HELM is invalid and we cannot continue."""


class MonomerLibraryError(Exception):
    """Unable to read monomer file."""


class MonomerLibrary:
    Format = Enum("Format", ["NONE", "PICKLE_GZ", "PISTOIA_JSON", "DICT"])

    def __init__(self, path: Path):
        self.format = MonomerLibrary.Format.NONE
        if path.suffix == ".json":
            try:
                with open(path, "r") as file:
                    self.data = json.load(file)
                    self.format = MonomerLibrary.Format.PISTOIA_JSON
            except json.JSONDecodeError:
                with open(path, "r") as file:
                    self.data = MonomerLibrary.parse_dict(file)
                    self.format = MonomerLibrary.Format.DICT
        elif path.suffixes == [".pkl", ".gz"]:
            self.data = pickle.load(gzip.open(path, "rb"))
            self.format = MonomerLibrary.Format.PICKLE_GZ

        if self.format == MonomerLibrary.Format.NONE:
            raise MonomerLibraryError(f"unable to parse monomer file '{path}'.")

    @staticmethod
    def parse_dict(file: io.TextIOWrapper):
        data = []
        polymer_type = None
        for line in file.readlines():
            if "Monomers" in line:
                if "aas" in line:
                    polymer_type = "PEPTIDE"
                elif "sugars" in line or "bases" in line or "linkers" in line:
                    polymer_type = "RNA"
                elif "chems" in line:
                    polymer_type = "CHEM"
                continue

            symbol, monomer_string = line.split(":", 1)
            monomer_string = re.sub(r"(\w+):", r'"\1":', monomer_string)  # Replace unquoted keys
            monomer_string = re.sub(r",$|};$", r"", monomer_string)  # Remove trailing comma/bracket
            monomer_data = json.loads(monomer_string)
            monomer_data["polymerType"] = polymer_type
            data.append(monomer_data)

        return data

    def build_mol(self, symbol: str, polymer_type: Optional[str]) -> Optional[Mol]:
        if self.format == MonomerLibrary.Format.PISTOIA_JSON:
            for item in self.data:
                if item["symbol"] == symbol:
                    if polymer_type:
                        if item["polymerType"] == polymer_type:
                            return _mol_from_smiles(item["smiles"])
                    else:
                        return _mol_from_smiles(item["smiles"])
        elif self.format == MonomerLibrary.Format.PICKLE_GZ:
            # All monomers do not have types in this collection.
            # We can only search this database if it is the last one.
            # Otherwise we might return the wrong monomer that would have been correct
            # from a different collection.
            if symbol in self.data:
                return _mol_from_smiles(self.data[symbol])
        elif self.format == MonomerLibrary.Format.DICT:
            for item in self.data:
                if item["id"] == symbol:
                    if polymer_type:
                        if item["polymerType"] == polymer_type:
                            return _mol_from_mol_block(item["m"], item["at"])
                    else:
                        return _mol_from_mol_block(item["m"], item["at"])
        return None


class HelmParser:
    FILE = Path(__file__)
    SEQUENCE_PAT = re.compile(r"((?:PEPTIDE|CHEM|RNA)[0-9]+)\{(.*?)\}")
    CONNECTION_PAT = re.compile(
        r"((?:PEPTIDE|CHEM|RNA)[0-9]+),((?:PEPTIDE|CHEM|RNA)[0-9]+),([0-9]+):R([0-9]+)-([0-9]+):R([0-9]+)"  # noqa: E501
    )
    MONOMER_DIRECTORY = FILE.parent / "monomers"
    PolymerType = Enum("PolymerType", ["RNA", "PEPTIDE", "CHEM"])

    MONOMER_LIBS = []
    for filename in os.listdir(MONOMER_DIRECTORY):
        if os.path.isfile(MONOMER_DIRECTORY / filename) and filename.endswith(".json"):
            MONOMER_LIBS.append(MonomerLibrary(MONOMER_DIRECTORY / filename))

    # Add the .pkl.gz library last to ensure it gets searched after the others.
    for filename in os.listdir(MONOMER_DIRECTORY):
        if os.path.isfile(MONOMER_DIRECTORY / filename) and filename.endswith(".pkl.gz"):
            MONOMER_LIBS.append(MonomerLibrary(MONOMER_DIRECTORY / filename))

    def parse(self, helm_string) -> Mol:
        """Parse the complete HELM string into an RDKit molecule."""
        _validate(helm_string)
        sequences, connections, _, _, _ = helm_string.split("$")

        self.global_res_count = 0
        output_mol = Chem.Mol()

        for sequence_string in HelmParser.SEQUENCE_PAT.finditer(sequences):
            tag = sequence_string.group(1)
            structure = sequence_string.group(2)

            polymer_type = _identify_tag_type(tag)
            if polymer_type == HelmParser.PolymerType.CHEM:
                mol = self._build_chem(structure)
            else:
                mol = self._build_polymer(polymer_type, structure)

            _assign_tag(mol, tag)
            output_mol = Chem.CombineMols(output_mol, mol)

        if connections:
            output_mol = _connect(output_mol, connections)

        output_mol = _cap(output_mol)
        output_mol = _cleanup(output_mol)

        return output_mol

    def _set_residue_info(self, mol: Mol, symbol: str):
        """Assign basic residue information."""
        residue_info = Chem.AtomPDBResidueInfo()
        residue_info.SetResidueName(symbol)
        self.global_res_count = self.global_res_count + 1
        residue_info.SetResidueNumber(self.global_res_count)

        [a.SetMonomerInfo(residue_info) for a in mol.GetAtoms()]

    def _build_polymer(self, type: PolymerType, structure: str) -> Mol:
        """Create a polymer chain from a sequence of monomers."""
        mol = None
        residue_index = 1  # Start from 1 to match HELM attachment 1-based indexing
        for symbol in structure.split("."):
            if type == HelmParser.PolymerType.PEPTIDE:
                connection_point, residues_added, resmol = _build_peptide_monomer(
                    residue_index, symbol
                )
            elif type == HelmParser.PolymerType.RNA:
                connection_point, residues_added, resmol = _build_rna_monomer(residue_index, symbol)
            else:
                raise InvalidHelmError(f"cannot parse polymer type {type}.")
            residue_index += residues_added

            resmol = Chem.RWMol(resmol)

            self._set_residue_info(resmol, symbol)

            if mol:
                _mark_connections(mol, connection_point)
                _mark_connections(resmol, 1)
                mol = Chem.RWMol(Chem.CombineMols(mol, resmol))
                _connect_conns(mol)
            else:
                mol = resmol

        if mol:
            return mol.GetMol()

    def _build_chem(self, symbol: str) -> Mol:
        """Build a simple monomer from a SMILES string."""
        if "." in symbol:
            raise InvalidHelmError(
                f'CHEM polymers cannot contain more than one monomer: "{symbol}"'
            )
        symbol = _strip_brackets(symbol, "[", "]")
        mol = _mol_from_symbol(symbol, None)
        _assign_res_idx(mol, 1)
        self._set_residue_info(mol, symbol)
        return mol


def _build_peptide_monomer(residue_index: int, symbol) -> Tuple[int, int, Mol]:
    """Build an amino acid residue for the PEPTIDE chain."""
    symbol = _strip_brackets(symbol, "[", "]")
    mol = _mol_from_symbol(symbol, "PEPTIDE")
    _assign_res_idx(mol, residue_index)
    return 2, 1, mol


def _build_rna_monomer(residue_index: int, symbol: str) -> Tuple[int, int, Mol]:
    """Create a single nucleotide from the RNA chain.
    May consist of between 1 and 3 monomers."""
    groups = _group_chunks(symbol)
    mol = None
    connection_point = 2
    for count, symbol in enumerate(groups, start=residue_index):
        is_branch = symbol.startswith("(") and symbol.endswith(")")
        symbol = _strip_brackets(symbol, "(", ")")
        symbol = _strip_brackets(symbol, "[", "]")
        resmol = _mol_from_symbol(symbol, "RNA")

        _assign_res_idx(resmol, count)

        resmol = Chem.RWMol(resmol)

        if mol:
            if is_branch:
                root = 3
            else:
                root = 2
            _mark_connections(mol, root)
            _mark_connections(resmol, 1)
            mol = Chem.RWMol(Chem.CombineMols(mol, resmol))
            _connect_conns(mol)
        else:
            if is_branch:
                connection_point = 3
            mol = resmol

    if not mol:
        raise InvalidHelmError(f'unable to build nucleotide from "{symbol}".')

    return connection_point, len(groups), mol.GetMol()


def _validate(helm_string: str):
    """Identify any common failure cases."""

    dollars = helm_string.count("$")
    if dollars > 4:
        raise InvalidHelmError(
            "must contain exactly four '$' symbols. Inline monomers must be specified using "
            + "atom mapped SMILES, extended SMILES is not supported."
        )
    elif dollars < 4:
        raise InvalidHelmError("must contain exactly four '$' symbols.")

    components = helm_string.split("$")
    simple_polymers = components[0]
    if not HelmParser.SEQUENCE_PAT.match(simple_polymers):
        raise InvalidHelmError(f'no valid sequences detected in "{simple_polymers}"')

    if "pair" in helm_string:
        raise InvalidHelmError("hydrogen bonding connections are not supported.")


def _connect(mol, connections) -> Mol:
    """Apply the polymer connections specified in the second HELM section."""

    def _check_atom(tag: str, res: int) -> Callable[[Atom], bool]:
        return lambda atom: atom.GetProp("Tag") == tag and atom.GetIntProp("res_idx") == res

    if not connections:
        return mol

    mol = Chem.RWMol(mol)
    for conn in connections.split("|"):
        m = HelmParser.CONNECTION_PAT.match(conn)
        assert m
        tag1, tag2 = m.group(1), m.group(2)
        res1, res2 = int(m.group(3)), int(m.group(5))
        n1, n2 = int(m.group(4)), int(m.group(6))

        _mark_connections(mol, n1, _check_atom(tag1, res1))
        _mark_connections(mol, n2, _check_atom(tag2, res2))
        _connect_conns(mol)
    return mol.GetMol()


def _cap(mol: Mol) -> Mol:
    """Cap the molecule with H or OH groups."""
    for atom in mol.GetAtoms():
        if atom.GetAtomicNum() == 0:
            atom.SetAtomicNum(1)
            neighbor = atom.GetNeighbors()[0]
            if neighbor.GetAtomicNum() == 6:
                if atom.HasProp("Tag"):
                    if "CHEM" in atom.GetProp("Tag"):
                        continue
                atom.SetAtomicNum(8)
                atom.SetNumExplicitHs(1)
    mol = Chem.RemoveHs(mol)
    return mol


def _cleanup(mol: Mol) -> Mol:
    """Compute coordinates and remove left-over properties."""
    Compute2DCoords(mol)
    for atom in mol.GetAtoms():
        keys = atom.GetPropsAsDict(includePrivate=True).keys()
        for k in keys:
            atom.ClearProp(k)
    return mol


def _connect_conns(mol: Mol):
    """Find atoms labelled with 'conn' and join them with a single bond."""
    conns = []
    for atom in mol.GetAtoms():
        if atom.HasProp("conn") and atom.GetBoolProp("conn"):
            atom.ClearProp("conn")
            conns.append(atom.GetIdx())
    mol.AddBond(*conns, Chem.BondType.SINGLE)


def _assign_res_idx(mol: Mol, n: int):
    """Set the 'res_idx' property for all atoms in the molecule `mol`.
    Used to identify connections."""
    for atom in mol.GetAtoms():
        atom.SetIntProp("res_idx", n)


def _assign_tag(mol: Mol, tag: str):
    """Set the 'Tag' property for all atoms in the molecule `mol`.
    Used to identify connections."""
    for atom in mol.GetAtoms():
        atom.SetProp("Tag", tag)


def _mol_from_smiles(smiles: str, removeHs: bool = False) -> Mol:
    """Use RDKit to create a molecule from SMILES.
    Ensures atoms are mapped correctly."""
    if not _are_atoms_mapped(smiles):
        raise InvalidHelmError(f"connections in {smiles} are not numbered.")

    smiles_params = Chem.SmilesParserParams()
    smiles_params.removeHs = removeHs
    mol = Chem.MolFromSmiles(smiles, smiles_params)

    if mol is None:
        raise InvalidHelmError(f'failed to create monomer from SMILES "{smiles}".')

    return mol


def _mol_from_mol_block(mol_block: str, r_group_elements: dict) -> Optional[Mol]:
    mol = Chem.MolFromMolBlock(mol_block, removeHs=False)
    map_number = 1
    for atom in mol.GetAtoms():
        match = re.match(r"R(\d)", atom.GetSymbol())
        if match:
            atom.SetAtomMapNum(int(match.group(1)))
        elif atom.GetSymbol() == "R":
            # If the atoms are just labelled R, increment a simple counter
            atom.SetAtomMapNum(map_number)
            map_number += 1

    for atom in mol.GetAtoms():
        if atom.GetAtomMapNum() != 0:
            symbol = r_group_elements["R" + str(atom.GetAtomMapNum())]
            if symbol == "OH":
                symbol = "O"
            atom.SetAtomicNum(Chem.GetPeriodicTable().GetAtomicNumber(symbol))

    return mol


def _strip_brackets(symbol: str, open: str, close: str) -> str:
    """Strip opening and closing characters if they are both present."""
    if symbol.startswith(open) and symbol.endswith(close):
        symbol = symbol[1:-1]
    return symbol


def _are_atoms_mapped(smiles: str) -> bool:
    """Check that all wildcards are atom mapped."""
    matches = re.findall(r"\*:\d+", smiles)
    asterisk_count = smiles.count("*")
    return len(matches) == asterisk_count


def _group_chunks(s) -> List[str]:
    """Group chunks in RNA monomer.

    Takes an input of R(C)P and returns ['R', "(C)", "P"].
    R, C and P can be simple monomers, multicharacter monomers or SMILES.
    The latter two must be bracketed with [].
    """
    positions = [(m.start(), m.group()) for m in re.finditer(r"[\[\]()]", s)]

    stack = []
    chunks = []
    last_pos = 0

    for pos, char in positions:
        if char in "([":
            if not stack:
                if last_pos < pos:
                    chunks.append(s[last_pos:pos])
                last_pos = pos
            stack.append((pos, char))
        elif char in ")]":
            if stack:
                start_pos, start_char = stack.pop()
                if (start_char == "[" and char == "]") or (start_char == "(" and char == ")"):
                    if not stack:
                        chunks.append(s[start_pos : pos + 1])  # noqa: E203
                        last_pos = pos + 1

    # Add remaining part of the string if any
    if last_pos < len(s):
        chunks.append(s[last_pos:])

    return chunks


def _mark_connections(
    mol: Mol, map_index: int, check_atom: Optional[Callable[[Atom], bool]] = None
):
    """Tag the relevant connections using the `conn` property.

    `check_atom` provides additional checks when searching the right atom."""
    attachment = None
    for atom in mol.GetAtoms():
        if atom.GetAtomMapNum() == map_index:
            if check_atom:
                if check_atom(atom):
                    attachment = atom
                    break
            else:
                attachment = atom
                break
    if not attachment:
        raise InvalidHelmError("failed to find attachment points.")
    attachment.GetNeighbors()[0].SetBoolProp("conn", True)
    mol.RemoveAtom(attachment.GetIdx())


def _mol_from_symbol(symbol: str, monomer_type: Optional[str]) -> Mol:
    """Build a molecule from its monomer symbol."""
    mol = None
    for lib in HelmParser.MONOMER_LIBS:
        mol = lib.build_mol(symbol, monomer_type)
        if mol:
            return mol
    if not mol:
        mol = _mol_from_smiles(symbol)
    return mol


def _identify_tag_type(tag: str) -> HelmParser.PolymerType:
    if tag.startswith("PEPTIDE"):
        return HelmParser.PolymerType.PEPTIDE
    elif tag.startswith("CHEM"):
        return HelmParser.PolymerType.CHEM
    elif tag.startswith("RNA"):
        return HelmParser.PolymerType.RNA
    else:
        raise InvalidHelmError("polymer type not recognized.")
