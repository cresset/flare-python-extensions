# -*- coding: utf-8 -*-
# Copyright (C) 2025 Cresset Biomolecular Discovery Ltd.
# Released under CC-BY-SA 4.0 (https://creativecommons.org/licenses/by-sa/4.0/).
# Originally downloaded from https://gitlab.com/cresset
from PySide2 import QtCore
import time
import requests

BASE_URL = "https://app.collaborativedrug.com/api/v1/vaults"


class SearcherSignals(QtCore.QObject):
    progress_update = QtCore.Signal(int)

    search_completed = QtCore.Signal(dict, set)
    search_terminated = QtCore.Signal()

    error_code = QtCore.Signal(requests.Response)


class SearchWorker(QtCore.QRunnable):
    """
    class used for running searches and retrieving data on a worker thread
    also used for retrieving saved searches
    """

    def __init__(self, api_key, vault_id, queries=None, saved_search_id=None):
        """
        stores the api key and vault id from the main thread
        if queries is stored, then this will be used for starting new searches
        if search_ids is stored, then this will be used for retrieving saved searches
        """
        super().__init__()
        self.api_key = api_key
        self.vault_id = vault_id
        self.queries = queries
        self.saved_search_id = saved_search_id
        self.search_ids = []
        self.signals = SearcherSignals()
        self.cancelled = False

    """
    pyside2 uses signals and slots to send data between threads

    progress_update is sent by the worker thread with an integer from 0-100 to update the progress
    bar finish is sent by the worker thread with a dictionary of data when the search is finished
    """

    @QtCore.Slot()
    def cancel_search(self):
        for id in self.search_ids:
            # cancels the search and returns nothing
            self.cancel_async_request(id)

        self.cancelled = True
        self.signals.search_terminated.emit()

        self.search_ids = []  # Remove all cancelled entries to prevent double cancellation

    def send_search_requests(self) -> list:
        """Sends the requests to CDD Vault to start the searches.
        Returns a list of the search ids, eliminating None ids given by failed requests.
        """
        if self.queries is not None:
            search_ids = [
                id
                for id in [self.async_search_request(query) for query in self.queries]
                if id is not None
            ]
        else:
            search_ids = [self.get_saved_search()]

        return search_ids

    def emit_progress_update(self, progress):
        if not self.cancelled:
            self.signals.progress_update.emit(progress)

    @QtCore.Slot()
    def run(self):
        """
        sets up an async search for each query in self.queries, and checks the status with cdd
        vault until it is finished
        """
        self.emit_progress_update(1)

        self.search_ids = self.send_search_requests()
        nsearches = len(self.search_ids)

        if nsearches == 0:
            self.signals.search_terminated.emit()
            return

        poll_interval = 0

        while not self.cancelled:
            # checks how many searches are complete
            completed_requests = self.check_async_requests(self.search_ids)

            if completed_requests == -1:
                self.cancel_search()
                return

            if completed_requests < nsearches:
                self.emit_progress_update(max(1, 100 * completed_requests / nsearches))

                # Cancellation cannot be detected whilst sleeping
                # can lead to a little delay after cancel is pressed.
                time.sleep(poll_interval)

                if poll_interval < 5:
                    poll_interval += 0.5

            else:
                self.emit_progress_update(99)
                self.return_results()
                return

        else:  # Execute if search is cancelled
            self.signals.search_terminated.emit()

    def return_results(self):
        """
        retrieves the data from the searches once all are complete
        """

        data, fields = self.gather_data()

        if self.cancelled:
            self.signals.search_terminated.emit()
        else:
            self.signals.search_completed.emit(data, fields)

    def gather_data(self):
        """
        formats data from all searches
        """
        data = {}

        search_results = [self.retrieve_async_search(id) for id in self.search_ids]

        # combines the data from each search so there are no duplicate molecules
        for result in search_results:
            # if there was an error retrieving the data, then the result will be None
            if result is None:
                continue
            for molecule_id, molecule_data in result.items():
                data[molecule_id] = molecule_data

        fields = set()
        # collects fields
        for datum in data.values():
            for field in datum["fields"]:
                if datum["fields"][field] not in ["", None]:
                    fields.add(field)

        # ensures each datum has each field
        for datum in data.values():
            for field in fields:
                if field not in datum["fields"]:
                    datum["fields"][field] = None

        return data, fields

    def get_saved_search(self):
        """
        returns the data from a saved search on cdd vault
        """

        url = "/".join([BASE_URL, str(self.vault_id), "searches", str(self.saved_search_id)])
        headers = {"X-CDD-token": self.api_key}
        query = {
            "format": "sdf",
        }
        try:
            response = requests.request("GET", url, json=query, headers=headers, timeout=45)
        except (ConnectionError, TimeoutError):
            return

        if response.status_code != 200:
            self.signals.error_code.emit(response)
            return
        search_id = response.json()["id"]
        return search_id

    def async_search_request(self, query):
        """
        runs an async search request to CDD Vault, returning the search id
        """

        if self.cancelled:
            return

        url = "/".join([BASE_URL, str(self.vault_id), "molecules"])
        headers = {"X-CDD-token": self.api_key}
        try:
            response = requests.request("GET", url, json=query, headers=headers)
        except (ConnectionError, TimeoutError):
            return

        if response.status_code != 200:
            self.signals.error_code.emit(response)
            self.cancel_worker()
            return
        search_id = response.json()["id"]
        return search_id

    def cancel_async_request(self, id):
        """
        cancels an async search request on cdd vault
        """

        url = "/".join([BASE_URL, str(self.vault_id), "exports", str(id)])
        headers = {"X-CDD-token": self.api_key}
        try:
            requests.request("DELETE", url, headers=headers)
            print(f"Search {id} cancelled.")
        except (ConnectionError, TimeoutError) as error:
            print(f"Failed to cancel search {id} with {error}.")
            return

    def check_async_requests(self, search_ids):
        """
        checks the status of multiple ongoing async search requests on cdd vault.
        returns the number of searches that have finished.
        """

        completed_searches = 0
        # loops over each search id provided
        for search_id in search_ids:
            url = "/".join([BASE_URL, str(self.vault_id), "export_progress", str(search_id)])
            headers = {"X-CDD-token": self.api_key}
            try:
                response = requests.request("GET", url, headers=headers)
            except (ConnectionError, TimeoutError):
                return -1

            if response.status_code != 200:
                self.signals.error_code.emit(response)
                return -1

            status = response.json()["status"]

            # if this search is finished, add 1 to the counter
            if status == "finished":
                completed_searches += 1

        return completed_searches

    def retrieve_async_search(self, search_id):
        """
        returns the data from an async search request on cdd vault
        """

        url = "/".join([BASE_URL, str(self.vault_id), "exports", str(search_id)])
        headers = {"X-CDD-token": self.api_key}
        try:
            response = requests.request("GET", url, headers=headers, timeout=45)
        except (ConnectionError, TimeoutError):
            return {}

        """
        example response format:
        {
            'count': 1,
            'objects':
            [
                {
                    'id': 101536404,
                    'class': 'molecule',
                    'created_at': '2021-09-24T19:57:39.000Z',
                    'modified_at': '2022-10-12T14:04:18.000Z',
                    'name': 'TRX-0000025',
                    'synonyms': ['TRX-0000025'],
                    'cdd_registry_number': 2613261,
                    'registration_type': 'CHEMICAL_STRUCTURE',
                    'projects':
                    [
                        {
                            'name': 'Main',
                            'id': 17754
                        }
                    ],
                    'owner': <owner_name>,
                    'smiles': 'Oc1c(Cl)c(Cl)c(Cl)c2ccccc12',
                    'cxsmiles': 'OC1=C(Cl)C(Cl)=C(Cl)C2=CC=CC=C12',
                    'inchi': 'InChI=1S/C10H5Cl3O/c11-7-5-3-1-2-4-6(5)10(14)9(13)8(7)12/h1-4,14H',
                    'inchi_key': 'RAJCVPYIEXLJLN-UHFFFAOYSA-N',
                    'iupac_name': '2,3,4-trichloro-1-naphthol',
                    'molfile': <molfile>,
                    'molecular_weight': 243.334,
                    'log_p': -1.1756,
                    'log_d': -1.20917,
                    'log_s': -0.462454,
                    'num_h_bond_donors': 2,
                    'num_h_bond_acceptors': 4,
                    'num_rule_of_5_violations': 0,
                    'formula': 'C14H19N4+',
                    'isotope_formula': 'C14H19N4+',
                    'p_k_a': 7.84,
                    'p_k_a_type': 'Basic',
                    'p_k_a_basic': 7.84,
                    'exact_mass': 243.16042302809,
                    'heavy_atom_count': 18,
                    'composition': 'C (69.10%), H (7.87%), N (23.03%)',
                    'isotope_composition': 'C (69.10%), H (7.87%), N (23.03%)',
                    'topological_polar_surface_area': 55.68,
                    'num_rotatable_bonds': 4,
                    'cns_mpo_score': 5.5,
                    'fsp3': 0.357143,
                    'batches':
                    [
                        {
                            'id': 17479034,
                            'class': 'batch',
                            'created_at': '2012-01-14T00:44:00.000Z',
                            'modified_at': '2012-01-14T00:44:00.000Z',
                            'owner': <owner_name>,
                            'projects': []
                        }
                    ],
                    'molecule_fields':
                    {
                        'MOLECULE_DATE': '5/9/2002'
                    },
                    'udfs':
                    {
                        'MOLECULE_DATE': '5/9/2002'
                    }
                }
            ]
        }
        """
        # expect 404 when using find_molecule with invalid name.
        if response.status_code == 404:
            return {}

        if response.status_code != 200:
            self.signals.error_code.emit(response)
            return {}

        if self.queries is None:
            molecules = self.format_from_sdf(response._content)
        else:
            molecules = response.json()["objects"]

        # basic data to extract from each molecule in the response
        BASIC_DATA = [
            "name",
            "molfile",
        ]

        # fields under "fields" in the response to store
        GENERIC_FIELDS = [
            # "molecular_weight", (provided by flare)
            "log_p",
            "log_d",
            "log_s",
            "num_h_bond_donors",
            "num_h_bond_acceptors",
            # "num_rule_of_5_violations", (provided by flare)
            "formula",
            "isotope_formula",
            "p_k_a",
            "p_k_a_type",
            "p_k_a_basic",
            "exact_mass",
            # "heavy_atom_count", (provided by flare)
            # "composition", (not useful)
            # "isotope_composition", (not useful)
            # "topological_polar_surface_area", (provided by flare)
            "num_rotatable_bonds",
            "cns_mpo_score",
            "fsp3",
        ]

        # formats the data, discarding irrelevant info
        data = {}
        for molecule in molecules:
            datum = {"fields": {}}
            # extracts data
            cont = False
            for key in BASIC_DATA:
                if key in molecule:
                    datum[key] = molecule[key]
                else:
                    cont = True
                    break
            if cont:
                continue

            # extracts fields and stores under fields
            for field in GENERIC_FIELDS:
                if field in molecule:
                    datum["fields"][field.replace("_", " ")] = molecule[field]
                else:
                    datum["fields"][field.replace("_", " ")] = None

            # extracts any custom fields and stores under fields
            if "molecule_fields" in molecule:
                for mol_field in molecule["molecule_fields"]:
                    datum["fields"][mol_field.replace("_", " ")] = molecule["molecule_fields"][
                        mol_field
                    ]
            data[molecule["id"]] = datum
        return data

    def format_from_sdf(self, sdf):
        """
        formats a sdf file into a dict
        """

        # CDD returns saved searches as either csv, sdf, or xlsx files, instead of their usual JSON
        # this function converts the sdf file into the usual format.

        # CDD formats the field names in the SDF file differently to the JSON.
        # the left is the JSON field names, and the right is the SDF field names.
        # this dict is used to convert between them.
        FIELDS = {
            "name": "Molecule Name",
            "molecular_weight": "Molecular weight (g/mol)",
            "log_p": "log P",
            "log_d": "log D",
            "log_s": "log S",
            "num_h_bond_donors": "H-bond donors",
            "num_h_bond_acceptors": "H-bond acceptors",
            "num_rule_of_5_violations": "Lipinski violations",
            "formula": "Chemical formula",
            "isotope_formula": "Isotope formula",
            "p_k_a": "pKa",
            "p_k_a_type": "pKa type",
            "p_k_a_basic": "pKa (Basic)",
            "exact_mass": "Exact mass (g/mol)",
            "heavy_atom_count": "Heavy atom count",
            "composition": "Composition",
            "isotope_composition": "Isotope composition",
            "topological_polar_surface_area": r"Topological polar surface area (\xc3\x85\xc2\xb2)",
            "num_rotatable_bonds": "Rotatable bonds",
            "cns_mpo_score": "CNS MPO score",
            "fsp3": "Fsp3",
        }

        # 4 dollar signs are used to delimit multiple molecules in an SDF
        molecules = [mol.split(r"\n") for mol in str(sdf).split(r"$$$$\n")]
        o = []

        for idx, mol in enumerate(molecules[:-1]):
            # SDFs contain a molfile, then have arbitrary data after it
            # molfile goes from start of the SDF until the text "M  END"
            i = mol.index("M  END") + 1
            molfile = "\n".join(mol[:i])

            # rest of the SDF is data items
            data_items = {
                # the first line in each data chunk is the title, which looks like this: > <title>
                # the initial > < is already removed by the split function in the for loop
                # slicing is used to remove the last arrow. the other lines are the data values,
                # usually only 1 line.
                item.split("\n")[0][:-1]: "\n".join(item.split("\n")[1:]).strip()
                # data items begin with "> <". These join and split functions groups the lines into
                # chunks of data
                for item in ("\n".join(mol[i:])).split("> <")[1:]
            }
            if idx == 0:
                print(data_items)
            # change field formatting
            entry = {key: data_items[value] for key, value in FIELDS.items() if value in data_items}
            # add molfile
            entry["molfile"] = molfile
            # add other fields
            entry["molecule_fields"] = {
                key: value for key, value in data_items.items() if key not in FIELDS.values()
            }

            # generate and add id
            entry["id"] = f"s-{str(idx)}"

            # append to output list
            o.append(entry)
        return o
