# -*- coding: utf-8 -*-
# Copyright (C) 2025 Cresset Biomolecular Discovery Ltd.
# Released under CC-BY-SA 4.0 (https://creativecommons.org/licenses/by-sa/4.0/).
# Originally downloaded from https://gitlab.com/cresset
from PySide2 import QtCore


class GeneralWorker(QtCore.QObject):
    """
    class used for performing any general function on a worker thread
    receives a function to run and a list of arguments to pass
    returns the return value(s) of the function
    """

    def __init__(self, func, args):
        """
        stores the function and arguments
        """
        super(GeneralWorker, self).__init__()
        self.func = func
        self.args = args

    """
    pyside2 uses signals and slots to send data between threads

    begin is sent by the main thread to start the function
    finish is sent by the worker thread with any object as the return value of the passed function
    raise_exception is sent by the worker thread with an exception to raise on the main thread
    """

    begin = QtCore.Signal()
    finish = QtCore.Signal(object)
    raise_exception = QtCore.Signal(Exception)

    @QtCore.Slot()
    def work(self):
        try:
            # executes the function, passing the unpacked arguments
            value = self.func(*self.args)

            # emits the value received from the function
            self.finish.emit(value)

        except Exception as exception:
            # exceptions raised in worker threads will not be shown in flare
            # sends a signal to the main thread to raise the exception
            self.raise_exception.emit(exception)
            self.finish.emit(None)
