# -*- coding: utf-8 -*-
# Copyright (C) 2025 Cresset Biomolecular Discovery Ltd.
# Released under CC-BY-SA 4.0 (https://creativecommons.org/licenses/by-sa/4.0/).
# Originally downloaded from https://gitlab.com/cresset

"""
Connects CDD Vault to Flare.

Ribbon Controls:
    CDD Vault -> Connection -> Connect
        Prompts the user to enter an API key and select a vault to connect to CDD Vault

    CDD Vault -> Search -> Similarity Search
        Searches CDD Vault for molecules similar to the selected ligands

    CDD Vault -> Search -> Substructure Search
        Searches CDD Vault for molecules that have one of the selected ligands as a substructure

    CDD Vault -> Search -> Find Molecule
        Prompts the user to enter a name and searches CDD Vault for a molecule of that name

    CDD Vault -> Import -> Import Molecules From Project
        Prompts the user to select a project or public data set on CDD Vault and imports all
        molecules in the project or data set

    CDD Vault -> Import -> Import Saved Search
        Prompts the user to select a saved search in CDD Vault and imports the molecules that the
        search returned

"""

import os
import requests
from rdkit import Chem
from typing import Optional
from cresset import flare
from PySide2 import QtCore, QtUiTools, QtGui, QtWidgets
from .general_worker import GeneralWorker
from .searcher import SearchWorker

extension_dir = os.path.dirname(os.path.realpath(__file__))


@flare.extension
class CDDVaultIntegrationExtension:
    def __init__(self):
        """
        initializes the extension
        """
        self.vault_id = None
        self.api_key = None
        self.projects = []
        self.selected_projects = []
        self.data_sets = []
        self.selected_data_sets = []
        self.connected = False
        self.base_url = "https://app.collaborativedrug.com/api/v1/vaults"

        # each button needs a group, text, function, icon, and tooltip
        self.ribbon_buttons = [
            {
                "group": "Connection",
                "text": "Connect",
                "function": self.enter_api_key,
                "icon": "connect.png",
                "tooltip": "Connect to CDD Vault",
            },
            {
                "group": "Search",
                "text": "Similarity Search",
                "function": self.similarity_search,
                "icon": "similarity search.png",
                "tooltip": "Search CDD Vault for structures similar to the selected ligand(s)",
            },
            {
                "group": "Search",
                "text": "Substructure Search",
                "function": self.substructure_search,
                "icon": "substructure search.png",
                "tooltip": "Search CDD Vault for structures with one of the selected ligands as "
                + "a substructure",
            },
            {
                "group": "Search",
                "text": "Find Molecules",
                "function": self.find_molecule,
                "icon": "find molecule.png",
                "tooltip": "Find molecules on CDD Vault by their names",
            },
            {
                "group": "Import",
                "text": "Import Molecules From Project",
                "function": self.import_project,
                "icon": "import project.png",
                "tooltip": "Import all molecules in a CDD Vault project",
            },
            {
                "group": "Import",
                "text": "Import Saved Search",
                "function": self.import_saved_search,
                "icon": "import saved search.png",
                # change this to the import saved search icon when it is ready
                "tooltip": "Import a saved search from CDD Vault",
            },
        ]

        self.flare_main_window_widget = flare.main_window().widget()
        self.thread_pool = QtCore.QThreadPool()

    def load(self):
        """
        triggered by flare when loading the extension
        creates the ribbon buttons and worker threads
        """

        # adds the buttons to the ribbon
        for button_data in self.ribbon_buttons:
            button = (
                flare.main_window()
                .ribbon["CDD Vault"][button_data["group"]]
                .add_button(button_data["text"], button_data["function"])
            )
            button.load_icon(os.path.join(extension_dir, "icons", button_data["icon"]))
            button.tooltip = button_data["tooltip"]

        # creates the general worker thread
        self.worker_thread = QtCore.QThread()
        # stores the active workers on the worker thread,
        # ensuring workers do not start until the previous worker has finished
        self.active_workers = []

        # separate thread and list so imports can work separately to other workers
        self.importer_thread = QtCore.QThread()
        self.active_importers = []
        self.active_imports = 0

        # storage for closed threads so they aren't collected by the garbage collector before
        # closing properly
        self.dead_threads = []

        print(f"Loaded {self.__class__.__module__}.{self.__class__.__name__}")

    def new_thread(self, thread):
        """
        closes the passed thread and returns a new thread to take its place
        """
        # clear the list of threads that have finished closing
        self.dead_threads = [thread for thread in self.dead_threads if thread.isRunning()]
        thread.quit()
        # threads do not close immediately and need to be referenced until they do.
        # add this thread to the list of dead threads
        self.dead_threads.append(thread)
        live_thread = QtCore.QThread()
        live_thread.setObjectName("CDD Vault Flare extension sub-thread")
        return live_thread

    def load_settings(self, settings):
        """
        loads the stored api key and vault id, along with selected data sets and projects
        """
        self.vault_id = settings.get("vault_id", None)
        self.api_key = settings.get("api_key", None)
        self.selected_projects = settings.get("selected_projects", [])
        self.selected_data_sets = settings.get("selected_data_sets", [])

        # checks that the loaded key/vault pair is valid
        self.verify_connection(None, True)

    def save_settings(self, settings):
        """
        saves the key/vault pair for next time the program is loaded, along with selected data sets
        and projects
        """
        settings["vault_id"] = self.vault_id
        settings["api_key"] = self.api_key
        settings["selected_projects"] = self.selected_projects
        settings["selected_data_sets"] = self.selected_data_sets

    def open_ui(self, ui_filename, modal=False):
        """
        opens a .ui file in the ui folder, returning the new window
        """
        file = QtCore.QFile(os.path.join(extension_dir, "ui", ui_filename))
        file.open(QtCore.QFile.ReadOnly)

        loader = QtUiTools.QUiLoader()
        parent_widget = self.flare_main_window_widget
        window = loader.load(file, parent_widget)

        if modal:
            window.setModal(True)
        else:
            window.show()
            # functions creating modal windows need to run window._exec() to block the thread and
            # show the window, instead of window.show()

        # this will automatically delete the window's memory when it is closed
        window.setAttribute(QtCore.Qt.WA_DeleteOnClose)

        return window

    def popup_window(self, message, title):
        """
        creates a simple window with a message and an ok button
        """
        window = self.open_ui("popup_window.ui")
        window.setWindowTitle(title)
        window.label.setText(message)

        @QtCore.Slot()
        def ok():
            window.close()

        window.okButton.clicked.connect(ok)

        return window

    def create_general_worker(
        self, worker_list, worker_thread, callback, func, *args, leave_thread=False
    ):
        """
        creates a general worker on the worker thread to execute the function func with arguments
        args, then calls the callback function with the return value of func as an argument set
        callback to none for no callback
        """

        @QtCore.Slot(object)
        def finish(values: object):
            # triggered when the worker finished executing the function

            # removes reference to worker
            worker_list.remove(worker)

            # if there are more workers, start the next worker in the queue
            if len(worker_list) > 0:
                worker_list[0].begin.emit()
            elif not leave_thread:
                # if there are no more workers queued, close the thread
                nonlocal worker_thread
                worker_thread = self.new_thread(worker_thread)
                # threads do not close immediately. There was a problem where workers were closing
                # threads, then new workers joined the thread while it was closing, as
                # thread.isRunning() returned true. For this reason, the thread is immediately
                # replaced with a new thread when it is closed, which is ready to be instantly
                # started, even if the current thread is still closing.

            # if a callback function was provided, calls the callback function
            # passes the return value as an argument
            if callback is not None:
                callback(values)

        @QtCore.Slot(Exception)
        def raise_exception(exception: Exception):
            # exceptions on sub threads aren't shown in flare
            # exceptions are caught and sent to the main thread to be raised
            raise exception

        # creates worker and moves it to the thread
        worker = GeneralWorker(func, args)
        worker.moveToThread(worker_thread)
        worker_list.append(worker)

        # connects signals and slots in the worker
        worker.begin.connect(worker.work)
        worker.finish.connect(finish)
        worker.raise_exception.connect(raise_exception)

        # begins the search if this searcher is the only one active.
        # otherwise, the searcher will wait until it is activated once the other searchers finish
        if len(worker_list) == 1:
            worker_thread.start()
            worker.begin.emit()

    def format_vault_info(self, info):
        """
        stores and formats the vault info
        """

        if len(info["projects"]) == 0:
            self.connected = False
            return

        self.projects = sorted(info["projects"], key=lambda x: x["name"])
        self.data_sets = sorted(info["data sets"], key=lambda x: x["name"])
        # alphabetise available projects and data sets

        self.selected_projects = [
            project for project in self.selected_projects if project in self.projects
        ]
        self.selected_data_sets = [
            data_set for data_set in self.selected_data_sets if data_set in self.data_sets
        ]
        # ensure selected projects and data sets contain only projects and data sets available to
        # the user

        if self.selected_projects == self.selected_data_sets == []:
            self.selected_projects = self.projects.copy()
        # if selected projects and data sets are both empty, set selected projects to all projects

    def verify_connection(self, callback=None, ignore_errors=False):
        """
        verifies an api key and vault id pair is valid
        """

        def finish(info):
            """
            triggered once get_vault_info has gathered various vault data
            """

            self.connected = True
            self.format_vault_info(info)

            if callback is not None:
                callback()

        def check_vault(vaults):
            """
            triggered once get_vaults_from_key has found a list of vaults accessible with the
            current api key. finds the vault that matches the vault id and runs get_vault_info in
            a sub thread
            """

            if vaults is None:
                self.connected = False
                return

            # iterates over the vaults to find the one that matches the current vault id
            for vault in vaults:
                if vault["id"] == self.vault_id:
                    # creates a worker to run get_vault_info in a sub thread
                    # passes finish as a callback
                    self.create_general_worker(
                        self.active_workers,
                        self.worker_thread,
                        finish,
                        self.get_vault_info,
                        ignore_errors,
                    )
                    return

            self.connected = False

        # creates a worker to run get_vaults_from_key in a sub thread
        # passes check_vault as a callback
        self.create_general_worker(
            self.active_workers,
            self.worker_thread,
            check_vault,
            self.get_vaults_from_key,
            self.api_key,
            ignore_errors,
        )

    def get_vaults_from_key(self, key, ignore_errors=False):
        """
        finds and returns the ids and names of all vaults accessible from a given api key
        """
        if key is None or key == "":
            return []

        url = self.base_url
        headers = {"X-CDD-token": key}
        try:
            response = requests.request("GET", url, headers=headers)
        except (ConnectionError, TimeoutError):
            if not ignore_errors:
                self.popup_window(
                    "Request failed. Please verify connection and try again", "Request Failed"
                )
            return []

        if response.status_code != 200 and not ignore_errors:
            # status code 401 is expected if the API key is invalid, so it is ignored
            self.handle_error_code(response, 401)
            return []

        vaults = response.json()
        return vaults

    def get_vault_info(self, ignore_errors=False):
        """
        returns a dict containing a list of projects and data sets
        """

        return {
            "projects": self.find_projects(ignore_errors),
            "data sets": self.find_data_sets(ignore_errors),
        }

    def find_projects(self, ignore_errors=False):
        """
        returns the list of projects the user has available
        """

        url = "/".join([self.base_url, str(self.vault_id), "projects"])
        headers = {"X-CDD-token": self.api_key}
        try:
            response = requests.request("GET", url, headers=headers)
        except (ConnectionError, TimeoutError):
            if not ignore_errors:
                self.popup_window(
                    "Request failed. Please verify connection and try again.", "Request Failed"
                )
            return

        if (response.status_code != 200) and (not ignore_errors):
            self.handle_error_code(response)
            return

        projects = response.json()
        return projects

    def find_data_sets(self, ignore_errors=False):
        """
        returns the list of data sets the user has available
        """

        url = "/".join([self.base_url, str(self.vault_id), "data_sets"])
        headers = {"X-CDD-token": self.api_key}
        try:
            response = requests.request("GET", url, headers=headers)
        except (ConnectionError, TimeoutError):
            if not ignore_errors:
                self.popup_window(
                    "Request failed. Please verify connection and try again.", "Request Failed"
                )
            return

        if (response.status_code != 200) and (not ignore_errors):
            self.handle_error_code(response)
            return

        data_sets = response.json()
        return data_sets

    def find_saved_searches(self):
        """
        returns the list of saved searches the user has available
        """

        url = "/".join([self.base_url, str(self.vault_id), "searches"])
        headers = {"X-CDD-token": self.api_key}
        try:
            response = requests.request("GET", url, headers=headers)
        except (ConnectionError, TimeoutError):
            self.popup_window(
                "Request failed. Please verify connection and try again.", "Request Failed"
            )
            return

        if response.status_code != 200:
            self.handle_error_code(response)
            return

        searches = response.json()
        return searches

    def enter_api_key(self, callback=None):
        """
        prompts the user to enter their api key to connect to the cdd vault

        triggered by other functions if they are run without a valid connection,
        which will provide themselves as a callback function.
        passes the callback function to the select_vaults function for it to be
        called when the user connects.
        """
        window = self.open_ui("enter_api_key.ui")
        window.setWindowTitle("Enter API Key")
        window.key.setText(self.api_key)

        @QtCore.Slot()
        def verify_key():
            # finds the vaults the user can access with the key they have entered
            key = window.key.text()
            window.hide()

            def show_vaults(vaults):
                # if the key has no vaults assigned to it, it is invalid
                if len(vaults) == 0:
                    window.show()
                    window.invalidWarning.setText("Invalid Key")

                else:
                    window.close()
                    # continues in a new function for readability
                    self.select_vault(vaults, key, callback)

            self.create_general_worker(
                self.active_workers,
                self.worker_thread,
                show_vaults,
                self.get_vaults_from_key,
                key,
            )

        window.findVaultsButton.clicked.connect(verify_key)

    def select_vault(self, vaults, key, callback=None):
        """
        triggered after the user enters their cdd vault api key
        prompts the user to select a vault
        if provided a callback function, will trigger it on successful connection
        """
        window = self.open_ui("select_vault.ui")
        window.setWindowTitle("Select Vault")

        # adds each vault to the combo box
        open_on_index = 0
        for index, vault in enumerate(vaults):
            window.vaultsComboBox.addItem(f'{vault["name"]}\n{vault["id"]}')
            if vault["id"] == self.vault_id:
                open_on_index = index
        # automatically selects the previous vault chosen, if it exists in the list
        window.vaultsComboBox.setCurrentIndex(open_on_index)

        @QtCore.Slot()
        def connect():
            nonlocal window
            self.vault_id = vaults[window.vaultsComboBox.currentIndex()]["id"]
            self.api_key = key
            window.close()
            # open the "Verifying..." window so the user knows something is happening
            window = VerifyingDialog(self.flare_main_window_widget)
            window.show()
            # verify the new connection
            self.verify_connection(finish)

        def finish():
            window.verification_complete(self.connected)
            if callback is not None:
                callback()

        window.connectButton.clicked.connect(connect)

    @QtCore.Slot()
    def select_projects(self):
        """
        opens the dialog for the user to select the projects and data
        sets to search through
        """
        window = self.open_ui("select_projects.ui", modal=True)
        window.setWindowTitle("Select Projects and Data Sets")

        project_checks, data_checks = [], []

        check_states = [[], []]

        ignoreChanges = True

        @QtCore.Slot()
        def on_select():
            nonlocal check_states, ignoreChanges

            # fetch checkbox states
            old_check_states = check_states.copy()
            check_states = [
                [check["checkbox"].checkState() for check in project_checks],
                [check["checkbox"].checkState() for check in data_checks],
            ]

            # modifying check states through script triggers this function, ignoreChanges ensures
            # no infinite loops
            if ignoreChanges:
                return

            ignoreChanges = True

            # compare checkbox states to find the changed checkbox
            for c, ref in zip(
                [*check_states[0], *check_states[1]], [*old_check_states[0], *old_check_states[1]]
            ):
                if c != ref:
                    check = c
                    break
            else:
                # if no checkboxes were changed, return
                return
            # set all selected cells to the checkbox's value
            if len(window.tableView.selectionModel().selectedIndexes()) > 1:
                for idx in window.tableView.selectionModel().selectedIndexes():
                    item = model.itemFromIndex(idx)
                    if item.isCheckable():
                        item.setCheckState(check)
            ignoreChanges = False

        # populate project table
        model = QtGui.QStandardItemModel()

        for idx, project in enumerate(self.projects):
            # create a checkbox in the table for each project
            item = QtGui.QStandardItem(project["name"])
            checkstate = (
                QtCore.Qt.Checked if project in self.selected_projects else QtCore.Qt.Unchecked
            )
            item.setCheckable(True)
            item.setCheckState(checkstate)
            project_checks.append({"checkbox": item, "project": project})
            model.setItem(idx, 0, item)
            check_states[0].append(checkstate)

        for idx, data_set in enumerate(self.data_sets):
            # create a checkbox in the table for each data set
            item = QtGui.QStandardItem(data_set["name"])
            checkstate = (
                QtCore.Qt.Checked if data_set in self.selected_data_sets else QtCore.Qt.Unchecked
            )
            item.setCheckable(True)
            item.setCheckState(checkstate)
            data_checks.append({"checkbox": item, "data_set": data_set})
            model.setItem(idx, 1, item)
            check_states[1].append(checkstate)

        ignoreChanges = False

        # format the table
        model.setHorizontalHeaderLabels(["Projects:", "Data Sets:"])
        window.tableView.setModel(model)
        window.tableView.verticalHeader().setVisible(False)
        window.tableView.horizontalHeader().setSectionsClickable(False)
        window.tableView.setShowGrid(False)
        window.tableView.setWordWrap(False)
        window.tableView.setEditTriggers(type(window.tableView).NoEditTriggers)
        window.tableView.resizeRowsToContents()
        window.tableView.resizeColumnsToContents()
        model.itemChanged.connect(on_select)

        def set_all_checks(state, check_list):
            nonlocal ignoreChanges
            for check in check_list:
                ignoreChanges = True
                check["checkbox"].setCheckState(QtCore.Qt.Checked if state else QtCore.Qt.Unchecked)
                ignoreChanges = False

        @QtCore.Slot()
        def select_all_projects():
            set_all_checks(True, project_checks)

        @QtCore.Slot()
        def deselect_all_projects():
            set_all_checks(False, project_checks)

        @QtCore.Slot()
        def select_all_data():
            set_all_checks(True, data_checks)

        @QtCore.Slot()
        def deselect_all_data():
            set_all_checks(False, data_checks)

        window.projectsSelectAllButton.clicked.connect(select_all_projects)
        window.projectsDeselectAllButton.clicked.connect(deselect_all_projects)
        window.dataSelectAllButton.clicked.connect(select_all_data)
        window.dataDeselectAllButton.clicked.connect(deselect_all_data)

        @QtCore.Slot()
        def done():
            selected_projects = [
                project["project"]
                for project in project_checks
                if project["checkbox"].checkState() == QtCore.Qt.Checked
            ]
            selected_data_sets = [
                data_set["data_set"]
                for data_set in data_checks
                if data_set["checkbox"].checkState() == QtCore.Qt.Checked
            ]

            if selected_projects == selected_data_sets == []:
                window.invalidWarning.setText("Select at least one project or data set")
                return

            self.selected_projects = selected_projects
            self.selected_data_sets = selected_data_sets
            window.close()

        @QtCore.Slot()
        def cancel():
            window.close()

        window.doneButton.clicked.connect(done)
        window.cancelButton.clicked.connect(cancel)
        window.exec_()

    def similarity_search(self):
        """
        runs a similarity search through cdd vault
        """
        self.general_search("similarity search", self.similarity_search)

    def substructure_search(self):
        """
        runs a substructure search through cdd vault
        """
        self.general_search("substructure search", self.substructure_search)

    def general_search(self, search_name, callback):
        """
        function for running substructure and similarity searches
        """
        selected_ligands = flare.main_window().selected_ligands

        # the user needs to select at least one ligand to perform the search on
        if len(selected_ligands) == 0:
            self.popup_window("No ligands selected", search_name.title())
            return

        # We support only a single search at a time.
        if len(selected_ligands) > 1:
            self.popup_window("Please select only a single ligand", search_name.title())
            return

        # checks if the user has a connection to cdd vault
        if not self.connected:
            # passes the original function as a callback
            # the callback will be run once the user establishes a connection
            self.enter_api_key(callback)
            return

        # creates the title for the search
        # this is used to name the role the ligands will be placed in when the search finishes
        search_title = f"{search_name.title()} for { ', '.join(ligand.title for ligand in selected_ligands)}"  # noqa E201

        window = self.open_ui(f"{search_name.replace(' ', '_')}.ui")
        window.setWindowTitle(search_name.title())
        window.label.setText(
            f"{search_name.capitalize()} on {len(selected_ligands)} "
            + f"{pluralise('ligand', len(selected_ligands), '', 's')}."
        )
        # sets the selected date to tomorrow's date
        window.toDateEdit.setDate(QtCore.QDate.currentDate().addDays(1))
        # set the maximum search date to tomorrow's date
        window.fromDateEdit.setMaximumDate(QtCore.QDate.currentDate().addDays(1))
        window.toDateEdit.setMaximumDate(QtCore.QDate.currentDate().addDays(1))

        window.selectProjectsButton.clicked.connect(self.select_projects)

        @QtCore.Slot()
        def begin_search():
            # generates queries for each ligand selected
            before_date = window.toDateEdit.date()
            after_date = window.fromDateEdit.date()
            queries = [
                {
                    "structure": structure.smiles(),
                    "structure_search_type": search_name.split()[0],
                    "async": "True",
                    "created_before": before_date.toString("yyyy-MM-dd"),
                    "created_after": after_date.toString("yyyy-MM-dd"),
                    "include_original_structures": "True",
                    "projects": ",".join(
                        [str(project["id"]) for project in self.selected_projects]
                    ),
                    "data_sets": ",".join(
                        [str(data_set["id"]) for data_set in self.selected_data_sets]
                    ),
                }
                for structure in selected_ligands
            ]
            # add a similarity threshold if the search is for similarity
            if search_name == "similarity search":
                threshold = window.thresholdDoubleSpinBox.value()
                for query in queries:
                    query["structure_similarity_threshold"] = threshold

            window.close()
            self.search(search_title, queries=queries)

        window.searchButton.clicked.connect(begin_search)

    def find_molecule(self):
        """
        searches cdd vault for molecules by name
        """

        # checks if the user has a connection to cdd vault
        if not self.connected:
            # passes this function as a callback
            # the callback will be run once the user establishes a connection
            self.enter_api_key(self.find_molecule)
            return

        window = FindMoleculeDialog(self.flare_main_window_widget)

        @QtCore.Slot()
        def begin_search():
            # creates the query
            query = {"names": window.moleculeID.text(), "async": "True"}

            # if use public data is checked, add the data sets to the query
            if window.publicDataCheckBox.checkState():
                query["data_sets"] = ", ".join([str(data_set["id"]) for data_set in self.data_sets])

            window.close()

            # search function expects a list of queries, so the query is put in a list
            queries = [query]

            # provides search function with a message to display
            # replaces the standard "retrieving data from 1 search" message
            label_text = f'Searching for molecule {"s" if "," in window.moleculeID.text() else ""}'
            for mol in window.moleculeID.text().split(","):
                label_text += f" '{mol}',"
            label_text = label_text[:-1]
            # begins the search
            self.search("Ligands", label_text, queries=queries)

        window.searchButton.clicked.connect(begin_search)
        window.show()

    def import_project(self):
        """
        imports an entire project into flare
        """
        # checks if the user has a connection to cdd vault
        if not self.connected:
            # passes the original function as a callback
            # the callback will be run once the user establishes a connection
            self.enter_api_key(self.import_project)
            return

        window = ImportProjectDialog(self.flare_main_window_widget)

        # populate projects combo box
        for project in self.projects:
            window.projectsComboBox.addItem(project["name"])

        @QtCore.Slot()
        def switch_data():
            """
            swaps the items in the combo box between data sets and projects when the
            "use public data" checkbox is checked and unchecked
            """
            window.projectsComboBox.clear()
            if window.publicDataCheckBox.checkState():
                for data_set in self.data_sets:
                    window.projectsComboBox.addItem(data_set["name"])
            else:
                for project in self.projects:
                    window.projectsComboBox.addItem(project["name"])

        window.publicDataCheckBox.clicked.connect(switch_data)

        # sets the selected date to tomorrow's date
        window.toDateEdit.setDate(QtCore.QDate.currentDate().addDays(1))
        # set the maximum search date to tomorrow's date
        window.fromDateEdit.setMaximumDate(QtCore.QDate.currentDate().addDays(1))
        window.toDateEdit.setMaximumDate(QtCore.QDate.currentDate().addDays(1))

        @QtCore.Slot()
        def begin_search():
            # generates the query and begins the search
            before_date = window.toDateEdit.date()
            after_date = window.fromDateEdit.date()
            queries = [
                {
                    "created_before": before_date.toString("yyyy-MM-dd"),
                    "created_after": after_date.toString("yyyy-MM-dd"),
                    "async": "True",
                    "include_original_structures": "True",
                }
            ]

            search_name = ""

            # add a data set query if use public data is checked
            if window.publicDataCheckBox.checkState():
                data_set = str(self.data_sets[window.projectsComboBox.currentIndex()]["id"])
                for query in queries:
                    query["data_sets"] = data_set
                    query["projects"] = ""
                search_name = str(self.data_sets[window.projectsComboBox.currentIndex()]["name"])
            # else, add a project query
            else:
                project = str(self.projects[window.projectsComboBox.currentIndex()]["id"])
                for query in queries:
                    query["projects"] = project
                search_name = str(self.projects[window.projectsComboBox.currentIndex()]["name"])

            label_text = f"Downloading project '{search_name}'"
            window.close()
            self.search(search_name, label_text, queries=queries)

        window.searchButton.clicked.connect(begin_search)
        window.show()

    def search(
        self,
        search_name: str,
        label_text: Optional[str] = None,
        queries: Optional[list[dict]] = None,
        saved_search_id=None,
    ):
        """
        function that handles all searches
        sets up a worker to run multiple async searches
        """

        window = self.open_ui("progress_bar.ui")
        window.setWindowTitle("Retrieving Data")
        # sets the label text unless one has been given as an input already
        if (label_text is None) and (queries is not None):
            label_text = f"Retrieving data from {len(queries)} " + pluralise(
                "search", len(queries), "", "es"
            )
        window.label.setText(label_text)

        @QtCore.Slot(int)
        def update_progress_bar(progress: int):
            window.progressBar.setValue(progress)

        @QtCore.Slot(dict, set)
        def finished_search(data: dict, fields: set):
            self.search_completed(data, search_name, fields)

        searcher = SearchWorker(
            self.api_key, self.vault_id, queries=queries, saved_search_id=saved_search_id
        )

        # connects signals and slots in the worker
        searcher.signals.progress_update.connect(update_progress_bar)
        searcher.signals.search_completed.connect(finished_search)
        searcher.signals.error_code.connect(self.handle_error_code)

        # Two outcomes that can close the window
        searcher.signals.search_terminated.connect(window.close)
        searcher.signals.search_completed.connect(window.close)

        window.cancelButton.clicked.connect(searcher.cancel_search)
        window.destroyed.connect(searcher.cancel_search)
        window.destroyed.connect(
            lambda: searcher.signals.progress_update.disconnect(update_progress_bar)
        )

        self.thread_pool.start(searcher)
        window.exec_()

    def import_saved_search(self):
        """
        imports a saved search from CDD Vault into Flare
        """
        if not self.connected:
            self.enter_api_key(self.import_saved_search)
            return

        window = self.open_ui("find_saved_search.ui")
        window.setWindowTitle("Fetching Saved Searches")

        cancelled = False

        def finish(searches):
            if not cancelled:
                window.close()
                self.select_saved_search(searches)

        def cancel():
            nonlocal cancelled
            window.close()
            cancelled = True

        self.create_general_worker(
            self.active_workers,
            self.worker_thread,
            finish,
            self.find_saved_searches,
        )

        window.cancelButton.clicked.connect(cancel)

    def select_saved_search(self, searches):
        window = self.open_ui("import_saved_search.ui")
        window.setWindowTitle("Select Search to Import")
        window.label.setText(
            f"Found {len(searches)} saved {pluralise('search', len(searches), '', 'es')}\n"
            + "Select search to import"
        )

        # populate searches combo box
        for search in searches:
            window.searchesComboBox.addItem(search["name"])

        selected_search = None

        # retrieve search requested
        @QtCore.Slot()
        def import_search():
            nonlocal selected_search
            selected_search = searches[window.searchesComboBox.currentIndex()]
            window.close()
            self.search(
                selected_search["name"],
                f"Retrieving data from search '{selected_search['name']}'",
                saved_search_id=selected_search["id"],
            )

        def cancel():
            window.close()

        window.cancelButton.clicked.connect(cancel)
        window.importButton.clicked.connect(import_search)

    def search_completed(self, data, search_name, fields):
        """
        runs after a search is completed, asks the user whether they
        would like to add the data to the flare workspace or discard it
        """
        if len(data) == 0:
            self.popup_window("Found 0 structures", "Search Completed")
            return

        window = self.open_ui("search_completed.ui")
        window.setWindowTitle("Search Completed")
        window.label.setText(
            f"Import {len(data)} {pluralise('structure', len(data), '', 's')}?\n"
            + f'Structures will be placed in the role "{search_name}"'
        )

        field_checks = []

        # populate field selector
        model = QtGui.QStandardItemModel()

        field_list = sorted(list(fields))

        if len(field_list) == 0:
            # if no fields are available, display this
            item = QtGui.QStandardItem("-- No available fields --")
            model.appendRow(item)
        else:
            for field in field_list:
                # add a checkbox to the field list for each field
                item = QtGui.QStandardItem(field)
                item.setCheckable(True)
                model.appendRow(item)
                field_checks.append(item)

        window.listView.setModel(model)

        def set_all_checks(state):
            for check in field_checks:
                check.setCheckState(QtCore.Qt.Checked if state else QtCore.Qt.Unchecked)

        @QtCore.Slot()
        def select_all():
            set_all_checks(True)

        @QtCore.Slot()
        def deselect_all():
            set_all_checks(False)

        window.selectAllButton.clicked.connect(select_all)
        window.deselectAllButton.clicked.connect(deselect_all)

        @QtCore.Slot()
        def on_import_button_clicked():
            """
            runs begin_import function when the import button is clicked
            """
            # create a list of selected fields
            selected_fields = []
            for idx, check in enumerate(field_checks):
                if check.checkState():
                    selected_fields.append(field_list[idx])
            # check if the "overwrite copies" checkbox is checked
            overwrite = window.overwriteCopiesCheckBox.checkState() == QtCore.Qt.Checked
            window.close()
            # moved to new function for readability
            self.begin_import(search_name, data, selected_fields, overwrite)

        window.importButton.clicked.connect(on_import_button_clicked)

        def discard():
            # if the user chooses to discard the data, close the window
            window.close()

        window.discardButton.clicked.connect(discard)

    def begin_import(self, search_name, data, selected_fields, overwrite):
        """
        imports the data received from the search as ligands in flare under a new role category
        """

        # creates fields if they don't already exist
        f = selected_fields.copy()
        for field in selected_fields:
            if field not in flare.main_window().project.ligands.columns.user_keys():
                if field in flare.main_window().project.ligands.columns:
                    # if the field exists but is not a user field, do not use this field
                    f.remove(field)
                else:
                    flare.main_window().project.ligands.columns.append(field)
        selected_fields = f

        # formats the data in a list instead of a dict and removes unselected fields
        data = [
            {
                "molfile": datum["molfile"],
                "name": datum["name"],
                "fields": {
                    key: value for key, value in datum["fields"].items() if key in selected_fields
                },
            }
            for datum in data.values()
        ]

        window = self.open_ui("progress_bar.ui")
        window.setWindowTitle("Importing Ligands")
        window.label.setText(
            f"Imported 0 of {len(data)} {pluralise('structure', len(data), '', 's')}"
        )

        # checks if a role already exists with the name we want to use
        # either creates the role or gets the reference to the existing one
        if search_name in flare.main_window().project.roles:
            role = flare.main_window().project.roles[search_name]
        else:
            role = flare.main_window().project.roles.append(search_name)

        """
        large imports can take a long time, so a sub thread is used so the program doesn't freeze
        while importing.
        new ligands in flare can only be added to the ligand list from the main thread.
        so, all the worker thread can do is format the data into a molfile before it is passed back
        to the main thread to import.
        when the main thread receives the data, it starts a new worker to format the next molecule,
        and repeats the loop.
        this means that the user can still use flare (though it will be very laggy) while the
        import is occurring, and flare will not freeze.
        the import uses a separate queue and thread as searches, so new searches don't have to wait
        until imports are finished.
        """

        @QtCore.Slot()
        def cancel():
            # cancels the import by closing the window
            # the import will detect the window is closed and cancel
            window.close()

        window.cancelButton.clicked.connect(cancel)

        dat = None

        def generate_mol_block(index):
            # formats molecule as a molfile on the import worker thread
            nonlocal dat
            datum = data[index]
            if datum["molfile"] is not None:
                structure = Chem.rdmolfiles.MolFromMolBlock(datum["molfile"])
            else:
                # if there is no structure, mark the import as invalid
                dat = {"index": index}
                return False
            dat = {
                "structure": structure,
                "name": datum["name"],
                "index": index,
                "fields": datum["fields"],
            }
            return True

        def import_ligand(is_valid):
            # imports the molecule into flare on the main thread
            mol = dat.copy()
            if is_valid:
                # create a new ligand or fetch the ligand to be overwritten
                if overwrite:
                    for check_ligand in role.ligands:
                        if mol["name"] == check_ligand.title:
                            ligand = check_ligand
                            break
                    else:
                        ligand = role.ligands.new()
                else:
                    ligand = role.ligands.new()

                # set name (title) and structure of the new ligand
                ligand.from_rdmol(mol["structure"])
                ligand.title = mol["name"]

                # update progress bar
                window.label.setText(
                    f"Imported {mol['index'] + 1} of {len(data)} "
                    + pluralise("structure", len(data), "", "s")
                )
                window.progressBar.setValue(100 * (mol["index"] + 1) / len(data))

                for field in selected_fields:
                    # write field data to ligand properties
                    ligand.properties[field].value = mol["fields"][field]

            # loop to the next molecule if there are more to import
            if len(data) - 1 > mol["index"] and window.isVisible():
                self.create_general_worker(
                    self.active_importers,
                    self.importer_thread,
                    import_ligand,
                    generate_mol_block,
                    mol["index"] + 1,
                    leave_thread=True,
                )
            else:
                self.active_imports -= 1
                if self.active_imports == 0:
                    self.importer_thread = self.new_thread(self.importer_thread)
                window.close()

        # begin the loop of importing each molecule
        # generates the molblock of the first molecule in the list on the worker thread
        self.importer_thread.start()
        self.active_imports += 1
        self.create_general_worker(
            self.active_importers,
            self.importer_thread,
            import_ligand,
            generate_mol_block,
            0,
            leave_thread=True,
        )

    @QtCore.Slot(requests.Response)
    def handle_error_code(self, response: requests.Response, *ignore_codes):
        """
        handles status codes from connecting to cdd vault.
        """

        if response.status_code in ignore_codes:
            return

        error_message = ""

        if response.status_code == 400:
            # 400 appears from malformed json syntax
            # this should never be received
            raise Exception("Error 400: Bad Request")
        elif response.status_code == 401:
            # 401 can only occur if the user loses access to the vault after starting flare and
            # connecting.
            error_message = "Error 401: Invalid API Key!"
            self.connected = False
        elif response.status_code == 403:
            # 403 appears when cdd vault denies a request
            # it should have an error in the response describing the issue
            error_message = "Error 403: Request denied by CDD Vault.\n"
        elif response.status_code == 404:
            # 404 appears from a bad URL
            # this should never be received
            raise Exception("Error 404: Page not found.")
        elif response.status_code == 408:
            # 408 is the status code for request timeout
            # this most often happens when the user is disconnected
            error_message = (
                "Connection failed. Please verify your internet connection before trying again"
            )
        elif response.status_code == 500:
            # 500 is an internal server error from cdd vault
            # it might have an error in the response describing the issue
            error_message = "Error 500: Something went wrong with CDD Vault.\n"
        elif response.status_code == 503:
            # 503 occurs when the cdd vault servers are too busy
            error_message = "Error 503: CDD Vault servers are too busy, please try again later."
        else:
            error_message = f"Error {response.status_code}"
        if "error" in response.json():
            error_message += "\n" + str(response.json()["error"])
        self.popup_window(error_message, f"Error {response.status_code}")


def pluralise(noun, count, singular_suffix, plural_suffix):
    """
    pluralize a noun if the count is not 1
    """
    if count == 1:
        return noun + singular_suffix
    return noun + plural_suffix


class ImportProjectDialog(QtWidgets.QDialog):
    def __init__(self, parent: QtWidgets.QWidget):
        super(ImportProjectDialog, self).__init__(parent)

        self.setWindowTitle("Import Project")
        description = QtWidgets.QLabel("Download all molecules in the selected project.")

        self.fromDateEdit = QtWidgets.QDateEdit()
        self.fromDateEdit.setCalendarPopup(True)
        self.toDateEdit = QtWidgets.QDateEdit()
        self.toDateEdit.setCalendarPopup(True)
        self.projectsComboBox = QtWidgets.QComboBox()
        self.publicDataCheckBox = QtWidgets.QCheckBox()
        self.searchButton = QtWidgets.QPushButton("Search")

        formLayout = QtWidgets.QFormLayout()
        formLayout.addRow("Search from:", self.fromDateEdit)
        formLayout.addRow("Search until:", self.toDateEdit)
        formLayout.addRow("Project:", self.projectsComboBox)
        formLayout.addRow("Use public data:", self.publicDataCheckBox)

        searchLayout = QtWidgets.QHBoxLayout()
        searchLayout.addStretch(1)
        searchLayout.addWidget(self.searchButton)

        layout = QtWidgets.QVBoxLayout()
        layout.addWidget(description)
        layout.addLayout(formLayout)
        layout.addLayout(searchLayout)
        self.setLayout(layout)


class FindMoleculeDialog(QtWidgets.QDialog):
    def __init__(self, parent: QtWidgets.QWidget):
        super(FindMoleculeDialog, self).__init__(parent)

        self.setWindowTitle("Find Molecule")
        description = QtWidgets.QLabel("Enter a comma separated list of molecule names.")
        description.setWordWrap(True)

        self.moleculeID = QtWidgets.QLineEdit()
        self.publicDataCheckBox = QtWidgets.QCheckBox()
        self.searchButton = QtWidgets.QPushButton("Search")

        formLayout = QtWidgets.QFormLayout()
        formLayout.addRow("Use public data:", self.publicDataCheckBox)

        searchLayout = QtWidgets.QHBoxLayout()
        searchLayout.addStretch(1)
        searchLayout.addWidget(self.searchButton)

        layout = QtWidgets.QVBoxLayout()
        layout.addWidget(description)
        layout.addWidget(self.moleculeID)
        layout.addLayout(formLayout)
        layout.addLayout(searchLayout)
        self.setLayout(layout)


class VerifyingDialog(QtWidgets.QDialog):
    def __init__(self, parent: QtWidgets.QWidget):
        super(VerifyingDialog, self).__init__(parent)

        self.setWindowTitle("Verifying Connection")

        self.text = QtWidgets.QLabel("Verifying...")
        self.text.setAlignment(QtCore.Qt.AlignCenter)
        self.text.setSizePolicy(
            QtWidgets.QSizePolicy.MinimumExpanding, QtWidgets.QSizePolicy.MinimumExpanding
        )

        self.ok_button = QtWidgets.QPushButton("Ok")
        self.ok_button.clicked.connect(self.close)
        self.ok_button.setVisible(False)
        self.ok_button.setSizePolicy(QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Minimum)

        layout = QtWidgets.QVBoxLayout()
        layout.addWidget(self.text)
        layout.addWidget(self.ok_button)
        self.setLayout(layout)

    def verification_complete(self, connected: bool):
        if connected:
            self.text.setText("Connection successful.")
        else:
            self.text.setText("Connection failed.")

        self.ok_button.setVisible(True)
