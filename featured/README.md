# Featured Extensions

The featured extensions is a collection of free Python scripts which add powerful new functionality to Flare (TM). After installing these extensions, a new ribbon tab titled "Extensions" should appear in the Flare GUI, containing buttons to access this new functionality.

If you need help with any of these extensions or have suggestions of your own please contact [Cresset support](https://www.cresset-group.com/about-us/contact-us/).

## Prerequisites

Some extensions require additional Python packages which are not installed by Flare. These packages can be installed from the command line by using the pyflare executable:

> pyflare -m pip install --user -r featured/requirements.txt

Alternatively, these packages can be installed from within Flare "Python Console" window.

```python
import sys
import subprocess
subprocess.check_call([sys.executable, "-m", "pip", "install", "--user", "-r", "featured/requirements.txt"])
```

Restart Flare to load the new packages.

## Installing

See the [installation guide](../README.md).

## Extensions Descriptions

### Calculate RMSD (calculatermsd)
Calculates the RMSD of ligands and poses using RDKit.

**New Ribbon Controls:**

*Extensions -> Ligand -> Calculate RMSD* - Calculates the RMSD between the selected ligand or pose and all the other ligands and poses with the same structure. A "RMSD" column will be added to the Ligands table with the results.


### Cresset Integration (cressetintegration)
Adds buttons and menus which allow other Cresset products to be access from Flare.

**New Context Menu Items:**

*Ligands table -> Send to Forge* - Open the selected molecules in Forge. This control is only shown if a Forge installation is detected.

*Ligands table -> Send to Spark* - Open the selected ligand to Spark. This menu item is only shown if a Spark installation is detected.


### Export Model Data (exportmodeldata)
Adds functions for exporting QSAR model data.

**New Ribbon Controls:**

*QSAR -> Import/Export -> QSAR Models -> Export Displayed QSAR Model Data* - Export the data used to build the displayed QSAR model.

*QSAR -> Import/Export -> QSAR Models -> Export Field Samples for Selected Ligands* - Export the field sample data for the selected ligands for use with external statistics packages.


### Loop Modeling (loopmodeling)
Predicts loop structures using PyFREAD and a specified database.

**New Ribbon Controls:**

*Sequences -> Homology Modeling -> Loop Modeling* - Predicts loop structures using the database search loop modeling method PyFREAD.

The calculation can run in automatic or manual mode.

Automatic mode runs on all selected proteins, all gaps in
the protein structure are found and a new protein is created
with the gaps filled in.

Manual mode generates multiple loops for a single gap in a protein structure.
It starts with one or more picked residues. The picked residues must all
be on the same chain of the same protein, and the loop is inserted between
the first and last picked residue on that chain, replacing everything in between
The specified N best generated loop models are loaded into
Flare as proteins. The anchor RMSD cutoff and substitution score cutoff
parameters may also be adjusted.

Databases can be downloaded from
https://www.cresset-group.com/support/support-resources/data-files-flare-python-api-extensions/
or generated using the scripts featured/loopmodeling/fread/fread_db_add.py and
featured/loopmodeling/fread/fread_db_optimise.py in the extensions
directory.

### Minimize Dynamics (minimizedynamics)
Copies the protein at various points in a dynamics timeline and minimizes them.

**New Ribbon Controls:**

*Extensions -> Protein -> Minimize Dynamics* - Makes copies of the protein at various points in the dynamics timeline and minimizes the them using the XED forcefield.


### PAINS (pains)
Run the PAINS filters on a set of ligands.

**New Ribbon Controls:**

*Ligand -> Table -> PAINS Filter* - Add a column to the ligand table which shows if each ligand passes or fails PAINS (Pan Assay INterference) filters.


### Plot (plot)
Adds a ligand scatter plot, Ramachandran plot and protein contact map plot to Flare.

**New Ribbon Controls:**

*Ligand -> Table -> Plot* - Open a scatter plot for 2 or 3 columns in the ligand table. The plot shows which ligands are selected and allows ligands in the plot to be selected by clicking them or drawing a lasso around them.

*Ligand -> Table -> Histogram* - Open a histogram plot for a column in the ligand table.

*Ligand -> Table -> Box Plot* - Open a boxplot for columns in the ligand table.

*Protein -> Structure -> Ramachandran* - Open a Ramachandran plot for the selected proteins. The plot shows which residues are picked and allows residues in the plot to be picked by clicking them or drawing a lasso around them.

*Protein -> Structure -> Contact Map* - Open a Protein Contact Map which shows the distance between the protein's alpha carbons.

*Home -> Appearance -> Color -> Color by Ramachandran* - Color the protein residues accordingly to which region they fall in the Ramachandran plot.

*Home -> Appearance -> Ribbon Color -> Color Ribbon by Ramachandran* - Color the ribbon accordingly to which region the residues fall in the the Ramachandran plot.


### QSAR Models (qsarmodels)
Adds a selection of machine learning QSAR extensions that run scikit-learn to produce models that are then converted to ONNX format to run predictions. Rather than adding buttons to the extensions ribbon tab, controls are added to the QSAR Model Building dialog under the Regression and Classification model types.

**New Ribbon Controls:**

*QSAR -> Build QSAR Model -> Model -> SVM* - Run either a Regression or Classification SVM model.

*QSAR -> Build QSAR Model -> Model -> Random Forest* - Run either a Regression or Classification Random Forest model.

*QSAR -> Build QSAR Model -> Model -> MLP* - Run either a Regression or Classification Multi-Layer Perceptron model.

*QSAR -> Build QSAR Model -> Model -> Gaussian Process* - Run a Regression Gaussian Process model.

*QSAR -> Build QSAR Model -> Model -> Consensus* - Run either a Regression or Classification Consensus model.


### 2D Sim (sim2d)
Calculates the 2D similarity of all ligands to the selected ligands.

**New Ribbon Controls:**

*Extensions -> Ligand -> Calculate 2D Sim* - Calculates the 2D Sim between the selected ligands and all the other ligands. A "2D Sim" column will be added to the Ligands table with the results.


### Total Formal Charge (totalformalcharge.py)
Adds a column to the ligand table which displays the ligand total formal charge.


### Tours (tours)
Adds guided tours of Flare's functionality.

**New Ribbon Controls:**

*Help -> Guided Tours -> Align Ligands* - Run a tour on how to align ligands.

*Help -> Guided Tours -> Dock Ligands* - Run a tour on how to dock ligands.

*Help -> Guided Tours -> Build QSAR* - Run a tour on how to build QSAR models.

*Help -> Guided Tours -> Build 2D QSAR* - Run a tour on how to build 2D QSAR models.

*Help -> Guided Tours -> Fit Ligands to a QSAR Model* - Run a tour on how to fit ligands to a QSAR model.

*Help -> Guided Tours -> Create Pharmacophore* - Run a tour on how to create a pharmacophore.

