# -*- coding: utf-8 -*-
# Copyright (C) 2025 Cresset Biomolecular Discovery Ltd.
# Released under CC-BY-SA 4.0 (https://creativecommons.org/licenses/by-sa/4.0/).
# Originally downloaded from https://gitlab.com/cresset

from PySide2.QtCore import Signal, Qt
from PySide2.QtWidgets import QDialog, QVBoxLayout, QListWidget, QDialogButtonBox, QListWidgetItem


class MultiStrSelDialog(QDialog):
    """Multiple string selection dialog, give a list of strings for the users
    to check in a list, then return checked strings from the list
    """

    new_strs = Signal(set)

    def __init__(self, parent, window_title, checked_list=True):
        super().__init__(parent)
        self.setWindowTitle(window_title)
        self.__checked_list = checked_list

        self.layout = QVBoxLayout(self)
        self.layout.addWidget(self._list_widget())
        self.layout.addWidget(self._buttons())

    def _list_widget(self) -> QListWidget:
        self.list = QListWidget()
        return self.list

    def _buttons(self) -> QDialogButtonBox:
        button_box = QDialogButtonBox(QDialogButtonBox.Ok | QDialogButtonBox.Cancel)
        button_box.accepted.connect(self.accept)
        button_box.rejected.connect(self.reject)
        return button_box

    def open_with(self, strs_list, pre_ticked={}):
        self.list.clear()
        for string in strs_list:
            item = QListWidgetItem(string, self.list)
            if self.__checked_list:
                item.setFlags(Qt.ItemIsUserCheckable | Qt.ItemIsEnabled)
                item.setCheckState(Qt.Unchecked if string not in pre_ticked else Qt.Checked)
        self.show()

    def accept(self):
        super().accept()
        if self.__checked_list:
            strs_set = set()
            for i in range(0, self.list.count()):
                item = self.list.item(i)
                if item.checkState() == Qt.Checked:
                    strs_set.add(item.text())
            if len(strs_set) > 0:
                self.new_strs.emit(strs_set)
        elif self.list.currentItem() is not None:
            self.new_strs.emit(self.list.currentItem().text())
