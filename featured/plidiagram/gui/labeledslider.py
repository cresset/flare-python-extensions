# -*- coding: utf-8 -*-
# Copyright (C) 2025 Cresset Biomolecular Discovery Ltd.
# Released under CC-BY-SA 4.0 (https://creativecommons.org/licenses/by-sa/4.0/).
# Originally downloaded from https://gitlab.com/cresset

from PySide2.QtCore import Qt, QSize
from PySide2.QtWidgets import QWidget, QSlider
from PySide2.QtGui import QPainter, QFont


class LabeledSlider(QWidget):
    def __init__(self, parent=None):
        super().__init__(parent=parent)

        self._labels = [-180, -90, 0, 90, 180]
        self._lefts = [2, 46, 97, 143, 181]
        self._bottom = 23
        self._slider = QSlider(Qt.Horizontal, self)
        self._slider.setRange(-180, 180)
        self._slider.setValue(0)
        self._slider.setTracking(False)
        self._slider.setTickPosition(QSlider.TicksBelow)
        self._slider.setMinimumWidth(200)
        self._slider.setPageStep(90)
        self._slider.setSingleStep(15)
        self._slider.setTickInterval(90)
        self._slider.setFocusPolicy(Qt.FocusPolicy.ClickFocus)

    def paintEvent(self, e):
        super().paintEvent(e)

        painter = QPainter(self)
        sansFont = QFont("Helvetica [Cronyx]", 6.5)
        painter.setFont(sansFont)

        for left, v in zip(self._lefts, self._labels):
            painter.drawText(left, self._bottom, str(v))

        painter.end()

        return

    def minimumSizeHint(self):
        return QSize(200, 25)
