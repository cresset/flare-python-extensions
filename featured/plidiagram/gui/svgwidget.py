# -*- coding: utf-8 -*-
# Copyright (C) 2025 Cresset Biomolecular Discovery Ltd.
# Released under CC-BY-SA 4.0 (https://creativecommons.org/licenses/by-sa/4.0/).
# Originally downloaded from https://gitlab.com/cresset

from cresset import flare
from PySide2.QtCore import Signal, QByteArray
from PySide2.QtSvg import QSvgWidget
from PySide2.QtGui import QMouseEvent
from .tooltip import PLOT_TOOLTIP
import numpy as np
import re


class SVGWidget(QSvgWidget):
    RESNAME_PAT = re.compile("([A-Z])([0-9]+)([A-Za-z]+)")  # Future support for nucleics

    clicked = Signal(QMouseEvent)

    def __init__(self, parent):
        super().__init__(parent=parent)
        self.parent = parent

        qbytes = QByteArray(self.parent.svg.encode("utf-8"))
        self.renderer().load(qbytes)

        self.setToolTip(PLOT_TOOLTIP)

        self.clicked.connect(self._highlight_residue)

    def mousePressEvent(self, event):
        self.clicked.emit(event)

    def _highlight_residue(self, event):
        main_window = flare.main_window()
        ligand = self.parent.ligand
        protein = self.parent.protein
        main_window.selected_ligands = [ligand]
        main_window.selected_proteins = [protein]

        clicked = np.array([event.pos().x(), event.pos().y()])

        found = False
        for res, coords in self.parent.res_coords.items():
            p = np.array([coords.x, coords.y])
            d2 = np.dot(p - clicked, p - clicked)
            if d2 <= 28 * 28:
                found = True
                break
        if not found:
            main_window.picked_atoms = []
            return
        match = self.RESNAME_PAT.match(res)
        chain = match.group(1)
        seq = match.group(2)
        name = match.group(3)
        ress = protein.residues.find(f"{name} {chain} {seq}")
        main_window.picked_atoms = ress[0].atoms
