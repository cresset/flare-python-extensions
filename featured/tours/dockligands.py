# -*- coding: utf-8 -*-
# Copyright (C) 2025 Cresset Biomolecular Discovery Ltd.
# Released under CC-BY-SA 4.0 (https://creativecommons.org/licenses/by-sa/4.0/).
# Originally downloaded from https://gitlab.com/cresset
from cresset import flare


class DockLigandsTour:
    """A tour on how to dock ligands."""

    def __init__(self):
        self._dock_tour()

    def _dock_tour(self):
        """Start the tour"""
        tour_widget = flare.tour.TourWidget()
        try:
            tour_widget.widget = flare.tour.find_widget("File")
            tour_widget.text = (
                "<b>Dock ligands in the active site of a protein</b><br>&nbsp;<br>"
                + "Before you start, check that the Ligands table is open.<br>&nbsp;<br>"
                + '<a href="protein_ligands_loaded">Protein and ligands to dock are already in the project.</a><br></a><br>'  # noqa: E501
                + '<a href="protein_loaded">The protein is in the project, but you have to load the ligands to dock.</a><br></a><br>'  # noqa: E501
                + '<a href="none_loaded">You have to load both the protein and ligands to dock.</a><br>&nbsp;<br>'  # noqa: E501
                + "<b>Note</b>: this tour does not cover Ensemble docking, "
                + "Covalent docking or Ensemble covalent docking workflows."
            )

            tour_widget.add_link_callback("protein_ligands_loaded", self._dock_proteinligandsin)
            tour_widget.add_link_callback("protein_loaded", self._dock_proteinin)
            tour_widget.add_link_callback("none_loaded", self._dock_nonein)
        except ValueError:
            tour_widget.text = (
                "You may have missed a step in the tour.<br>"
                + "Please start again and carefully follow the instructions."
            )

    # Option 1: protein and ligands to dock are already in the project

    def _dock_proteinligandsin(self):
        """Tell the user to select the ligands to dock."""
        tour_widget = flare.tour.TourWidget()
        try:
            tour_widget.widget = flare.tour.find_widget("Ligands")
            tour_widget.text = "Select all the ligands you want to dock."
            tour_widget.add_next_callback(self._dock_ligandtab)
        except ValueError:
            tour_widget.text = (
                "You may have missed a step in the tour.<br>"
                + "Please start again and carefully follow the instructions."
            )

    def _dock_proteinligandsin_ligandtab(self):
        """Tell the user to go to the 3D Pose tab."""
        tour_widget = flare.tour.TourWidget()
        try:
            tour_widget.widget = flare.tour.find_widget("3D Pose")
            tour_widget.text = "Go to the <b>3D Pose tab</b>."
            tour_widget.add_named_widget_shown_callback(["3D Pose"], self._dock_ligandtab)
        except ValueError:
            tour_widget.text = (
                "You may have missed a step in the tour.<br>"
                + "Please start again and carefully follow the instructions."
            )

    # Option 2: protein is in the project, ligands must be loaded

    def _dock_proteinin(self):
        """Tell the user to open the file menu."""
        tour_widget = flare.tour.TourWidget()
        try:
            tour_widget.widget = flare.tour.find_widget("File")
            tour_widget.text = "Press the <b>File</b> button to load the ligands to dock."
            tour_widget.add_named_widget_shown_callback(
                "Open File", self._dock_proteinin_open_file_menu
            )
        except ValueError:
            tour_widget.text = (
                "You may have missed a step in the tour.<br>"
                + "Please start again and carefully follow the instructions."
            )

    def _dock_proteinin_open_file_menu(self, widget):
        """Tell the user to press Open File in the File menu."""
        tour_widget = flare.tour.TourWidget()
        tour_widget.widget = widget
        tour_widget.text = "Press <b>Open File</b> to load the ligands to dock."
        tour_widget.add_named_widget_shown_callback(
            "Choose a file", self._dock_proteinin_choose_file
        )

    def _dock_proteinin_choose_file(self, widget):
        """Tell the user to choose a file."""
        tour_widget = flare.tour.TourWidget()
        tour_widget.widget = widget
        tour_widget.text = "Browse to find the ligands file and press <b>Open</b>."

        tour_widget.add_named_widget_shown_callback(
            "Open Molecules", self._dock_proteinin_ligands_open_file_options
        )
        tour_widget.add_named_widget_shown_callback(
            "CSV Import", self._dock_proteinin_ligands_open_csv
        )

    def _dock_proteinin_ligands_open_file_options(self, widget):
        """Tell the user to choose the import options for sdf/sd, mol, mol2, smi, pdd, xed files"""
        tour_widget = flare.tour.TourWidget()
        tour_widget.widget = widget
        tour_widget.text = (
            "Choose the input options for the ligands being imported.<br>&nbsp;<br>"
            + "1. The Molecule type should be 'Autodetect' or 'Ligands'.<br>&nbsp;<br>"
            + "2. Choose a role for the ligands.<br>"
            + "If you keep 'Autodetect', the ligands will be imported "
            + " in a role named after the file you are importing.<br>&nbsp;<br>"
            + "3. <b>Choose the Protonation state:</b><br>"
            + "Choose <b>Use input protonation state</b>"
            + " if you know that your ligands are in the correct protonation state.<br>"
            + "Choose <b>Apply rule-based protonation</b> "
            + "to let Flare define the protonation state.<br>&nbsp;<br>"
            + "4. The 'Ligand file read mode option' chooses how the input files are handled. "
            + "'Autodetect' works well in most cases.<br>&nbsp;<br>"
            + "Press <b>Open</b> to import your ligands."
        )

        tour_widget.add_named_widget_shown_callback(
            "Ligands", self._dock_proteinin_select_ligands_todock
        )

    def _dock_proteinin_ligands_open_csv(self, widget):
        """Tell the user to choose csv import options."""
        tour_widget = flare.tour.TourWidget()
        tour_widget.widget = widget
        tour_widget.text = (
            "Choose the input options for the ligands being imported.<br>&nbsp;<br>"
            + "1. Check the <b>Preview</b> panel to ensure that "
            + "the <b>Column Delimiter</b> is set correctly.<br>&nbsp;<br>"
            + "2. Choose a <b>Ligand Role</b> for the ligands, "
            + "or create a new role pressing the <b>Add Role"
            + " button</b>.<br>&nbsp;<br>"
            + "3. <b>Choose the Protonation state:</b><br>"
            + "Choose <b>Use input protonation state</b> "
            + " if you know that your ligands are in the correct protonation state.<br>"
            + "Choose <b>Apply rule-based protonation</b> "
            + "to let Flare define the protonation state.<br>&nbsp;<br>"
            + "4. Choose the columns in the CSV file which map the <b>Structure</b>,"
            + " and the <b>Ligand Name</b>.<br>&nbsp;<br>"
            + "Press <b>Open</b> to import your ligands."
        )

        tour_widget.add_named_widget_shown_callback(
            "Ligands", self._dock_proteinin_select_ligands_todock
        )

    def _dock_proteinin_select_ligands_todock(self, widget):
        """Tell the user to select the ligands to dock."""
        tour_widget = flare.tour.TourWidget()
        try:
            tour_widget.widget = flare.tour.find_widget("Ligands")
            tour_widget.text = (
                "Load additional ligands to align by pressing again File/Open File.<br>&nbsp;<br>"
                + "When you are done, select all the ligands you want to dock."
            )
            tour_widget.add_next_callback(self._dock_ligandtab)
        except ValueError:
            tour_widget.text = (
                "You may have missed a step in the tour.<br>"
                + "Please start again and carefully follow the instructions."
            )

    # Option 3: protein and ligands must be loaded

    def _dock_nonein(self):
        """Ask the users how they want to import they protein."""
        tour_widget = flare.tour.TourWidget()
        try:
            tour_widget.widget = flare.tour.find_widget("File")
            tour_widget.text = (
                "Choose how you want to import your protein:<br>&nbsp;<br>"
                + '<a href="openprotein">from a saved protein file</a><br>'
                + '<a href="downloadprotein">from a downloaded protein file.</a><br>'
            )
            tour_widget.add_link_callback("downloadprotein", self._dock_nonein_download_filetab)
            tour_widget.add_link_callback("openprotein", self._dock_nonein_filetab)
        except ValueError:
            tour_widget.text = (
                "You may have missed a step in the tour.<br>"
                + "Please start again and carefully follow the instructions."
            )

    # Load protein from file

    def _dock_nonein_filetab(self):
        """Tell the user to go to the File tab"""
        tour_widget = flare.tour.TourWidget()
        try:
            tour_widget.widget = flare.tour.find_widget("File")
            tour_widget.text = "Go to the <b>File</b> tab to open the protein file.<br>"
            tour_widget.add_named_widget_shown_callback(
                "Open File", self._dock_nonein_protein_openfile
            )
        except ValueError:
            tour_widget.text = (
                "You may have missed a step in the tour.<br>"
                + "Please start again and carefully follow the instructions."
            )

    def _dock_nonein_protein_openfile(self, widget):
        """Tell the user to press the Open File button"""
        tour_widget = flare.tour.TourWidget()
        tour_widget.widget = widget
        tour_widget.text = "Press the <b>Open File</b> button."
        tour_widget.add_named_widget_shown_callback(
            "Choose a file", self._dock_nonein_protein_choosefile
        )

    def _dock_nonein_protein_choosefile(self, widget):
        """Tell the user find the protein file they want to use"""
        tour_widget = flare.tour.TourWidget()
        tour_widget.widget = widget
        tour_widget.text = "Browse to find the protein file, then press <b>Open</b>.<br>"
        tour_widget.add_named_widget_shown_callback(
            ["Dialog", "Open Molecules"], self._dock_nonein_protein_prep
        )

    # Download protein

    def _dock_nonein_download_filetab(self):
        """Tell the user to go to the File tab"""
        tour_widget = flare.tour.TourWidget()
        try:
            tour_widget.widget = flare.tour.find_widget("File")
            tour_widget.text = "Go to the <b>File</b> tab to download the protein.<br>"
            tour_widget.add_named_widget_shown_callback(
                "Open File", self._dock_nonein_download_download
            )
        except ValueError:
            tour_widget.text = (
                "You may have missed a step in the tour.<br>"
                + "Please start again and carefully follow the instructions."
            )

    def _dock_nonein_download_download(self, widget):
        """Tell the user to press the Download button"""
        tour_widget = flare.tour.TourWidget()
        tour_widget.widget = widget
        tour_widget.text = "Press the <b>Download PDB</b> button."
        tour_widget.add_named_widget_shown_callback(
            "PDB Downloader", self._dock_nonein_download_pdbcodes
        )

    def _dock_nonein_download_pdbcodes(self, widget):
        """Tell the user type the pdb code of the protein they want to use"""
        tour_widget = flare.tour.TourWidget()
        tour_widget.widget = widget
        tour_widget.text = "Type the PDB code of the protein, then press <b>OK</b>.<br>"
        tour_widget.add_named_widget_shown_callback(
            ["Dialog", "Open Molecules"], self._dock_nonein_protein_prep
        )

    def _dock_nonein_protein_prep(self, widget):
        """Tell the user to prepare the protein"""
        tour_widget = flare.tour.TourWidget()
        tour_widget.widget = widget
        tour_widget.text = (
            "The Molecule type should be 'Autodetect' or 'Proteins'.<br>&nbsp;<br>"
            + "Before starting a docking experiment, it is highly "
            + "recommended to prepare your protein. "
            + "This will add hydrogen atoms; optimize hydrogen bonds; remove "
            + "atomic clashes and assign optimal protonation states.<br>&nbsp;<br>"
            + "To prepare your protein, set the Protonation state to "
            + "<b>Do full preparation on proteins and ligands</b>, "
            + "then press <b>Open</b>.<br>&nbsp;<br>"
            + "If your protein is already prepared, you can import it using "
            + "'Input protonation state': we will not do this in this tutorial."
        )
        tour_widget.add_named_widget_shown_callback(
            ["Protein Preparation", "Add Role"], self._dock_nonein_protein_prep_options
        )
        tour_widget.add_named_widget_shown_callback(
            "Protein", self._dock_nonein_protein_prep_not_selected
        )

    def _dock_nonein_protein_prep_not_selected(self, widget):
        """If the protein prep option was not selected tell the user to do it manually."""
        tour_widget = flare.tour.TourWidget()
        tour_widget.widget = widget
        tour_widget.text = (
            "You didn't choose "
            + "<b>Do full preparation on proteins and ligands</b> "
            + "in the previous step.<br>&nbsp;<br>"
            + " Let's prepare the protein manually. Go to the <b>Protein</b> tab."
        )
        tour_widget.add_named_widget_shown_callback(
            ["Protein", "Protein Prep"], self._dock_nonein_protein_protein_tab
        )

    def _dock_nonein_protein_protein_tab(self, widget):
        """Tell the user to open the protein tab."""
        tour_widget = flare.tour.TourWidget()
        tour_widget.widget = widget
        tour_widget.text = "Now press <b>Protein Prep.</b>"
        tour_widget.add_named_widget_shown_callback(
            ["Protein Preparation", "Add Role"], self._dock_nonein_protein_prep_options
        )

    def _dock_nonein_protein_prep_options(self, widget):
        """Tell the user to set the protein preparation options"""
        tour_widget = flare.tour.TourWidget()
        tour_widget.widget = widget
        tour_widget.text = (
            "Choose the protein preparation options.<br>&nbsp;<br>"
            + "Make sure you keep the <b>Auto-Extract Ligand</b> check box <b>ticked</b>, then "
            + "press <b>Start</b> to start the protein preparation."
        )
        tour_widget.add_named_widget_shown_callback(
            ["Window", "Flare", "Dock", "Calculations", "Details"],
            self._dock_nonein_protein_prep_progress,
        )

    def _dock_nonein_protein_prep_progress(self, widget):
        """Wait for protein prep to run."""
        tour_widget = flare.tour.TourWidget()
        tour_widget.widget = flare.tour.find_widget(
            ["Window", "Flare", "Dock", "Calculations", "Details"]
        )
        tour_widget.text = "Wait for the protein preparation to complete."
        tour_widget.add_widget_hidden_callback(self._dock_nonein_protein_prep_done)

    def _dock_nonein_protein_prep_done(self):
        """Tell the user that the protein preparation is complete."""
        tour_widget = flare.tour.TourWidget()
        try:
            tour_widget.widget = flare.tour.find_widget(["Ligands"])
            tour_widget.text = (
                "The protein has now been prepared, and "
                + "the ligand(s) in the original protein-ligand complex "
                + "have been moved to the Ligands table."
            )
            tour_widget.add_next_callback(self._dock_nonein_filemenu)
        except ValueError:
            tour_widget.text = (
                "You may have missed a step in the tour.<br>"
                + "Please start again and carefully follow the instructions."
            )

    def _dock_nonein_filemenu(self):
        """Tell the user to open the file menu."""
        tour_widget = flare.tour.TourWidget()
        try:
            tour_widget.widget = flare.tour.find_widget("File")
            tour_widget.text = "Press the <b>File</b> button to load the ligands to dock."
            tour_widget.add_named_widget_shown_callback(
                "Open File", self._dock_nonein_ligands_openfile
            )
        except ValueError:
            tour_widget.text = (
                "You may have missed a step in the tour.<br>"
                + "Please start again and carefully follow the instructions."
            )

    def _dock_nonein_ligands_openfile(self, widget):
        """Tell the user to press Open File in the File menu."""
        tour_widget = flare.tour.TourWidget()
        tour_widget.widget = widget
        tour_widget.text = "Press <b>Open File</b> to load the ligands to dock."
        tour_widget.add_named_widget_shown_callback(
            "Choose a file", self._dock_nonein_ligands_choose_file
        )

    def _dock_nonein_ligands_choose_file(self, widget):
        """Tell the user to choose a file."""
        tour_widget = flare.tour.TourWidget()
        tour_widget.widget = widget
        tour_widget.text = "Browse to find the ligand file and press <b>Open</b>."

        tour_widget.add_named_widget_shown_callback(
            "Open Molecules", self._dock_nonein_ligands_open_file_options
        )
        tour_widget.add_named_widget_shown_callback(
            "CSV Import", self._dock_nonein_ligands_open_csv
        )

    def _dock_nonein_ligands_open_file_options(self, widget):
        """Tell the user to choose the import options for sdf/sd, mol, mol2, smi, pdd, xed files"""
        tour_widget = flare.tour.TourWidget()
        tour_widget.widget = widget
        tour_widget.text = (
            "Choose the input options for the ligands being imported.<br>&nbsp;<br>"
            + "1. The Molecule type should be 'Autodetect' or 'Ligands'.<br>&nbsp;<br>"
            + "2. Choose a role for the ligands.<br>"
            + "If you keep 'Autodetect', the ligands will be imported "
            + " in a role named after the file you are importing.<br>&nbsp;<br>"
            + "3. <b>Choose the Protonation state:</b><br>"
            + "Choose <b>Use input protonation state</b>"
            + " if you know that your ligands are in the correct protonation state.<br>"
            + "Choose <b>Apply rule-based protonation</b> "
            + "to let Flare define the protonation state.<br>&nbsp;<br>"
            + "4. The 'Ligand file read mode option' chooses how the input files are handled. "
            + "'Autodetect' works well in most cases.<br>&nbsp;<br>"
            + "Press <b>Open</b> to import your ligands."
        )

        tour_widget.add_named_widget_shown_callback(
            "Ligands", self._dock_nonein_select_ligands_todock
        )

    def _dock_nonein_ligands_open_csv(self, widget):
        """Tell the user to choose csv import options."""
        tour_widget = flare.tour.TourWidget()
        tour_widget.widget = widget
        tour_widget.text = (
            "Choose the input options for the ligands being imported.<br>&nbsp;<br>"
            + "1. Check the <b>Preview</b> panel to ensure that "
            + "the <b>Column Delimiter</b> is set correctly.<br>&nbsp;<br>"
            + "2. Choose a <b>Ligand Role</b> for the ligands, "
            + "or create a new role pressing the <b>Add Role"
            + " button</b>.<br>&nbsp;<br>"
            + "3. <b>Choose the Protonation state:</b><br>"
            + "Choose <b>Use input protonation state</b> "
            + " if you know that your ligands are in the correct protonation state.<br>"
            + "Choose <b>Apply rule-based protonation</b> "
            + "to let Flare define the protonation state.<br>&nbsp;<br>"
            + "4. Choose the columns in the CSV file which map the <b>Structure</b>,"
            + " and the <b>Ligand Name</b>.<br>&nbsp;<br>"
            + "Press <b>Open</b> to import your ligands."
        )

        tour_widget.add_named_widget_shown_callback(
            "Ligands", self._dock_nonein_select_ligands_todock
        )

    def _dock_nonein_select_ligands_todock(self, widget):
        """Tell the user to select the ligands to dock."""
        tour_widget = flare.tour.TourWidget()
        try:
            tour_widget.widget = flare.tour.find_widget("Ligands")
            tour_widget.text = (
                "Load additional ligands to dock by pressing again File/Open File.<br>&nbsp;<br>"
                + "When you are done, select all the the ligands you want to dock."
            )
            tour_widget.add_next_callback(self._dock_ligandtab)
        except ValueError:
            tour_widget.text = (
                "You may have missed a step in the tour.<br>"
                + "Please start again and carefully follow the instructions."
            )

    # Do the docking

    def _dock_ligandtab(self):
        """Tell the user to press the Dock button."""
        tour_widget = flare.tour.TourWidget()
        try:
            tour_widget.widget = flare.tour.find_widget("Dock")
            tour_widget.text = "Press the <b>Dock</b> button."
            tour_widget.add_named_widget_shown_callback(
                ["Menu", "Normal"], self._dock_docking_normal
            )
        except ValueError:
            tour_widget.text = "Go to the <b>3D Pose tab</b>. "
            tour_widget.add_named_widget_shown_callback(["3D Pose", "Dock"], self._dock_docking)

    def _dock_docking(self, widget):
        """Tell the user to press the Dock button."""
        tour_widget = flare.tour.TourWidget()
        try:
            tour_widget.widget = flare.tour.find_widget(["3D Pose", "Dock"])
            tour_widget.text = "Press the <b>Dock</b> button."
            tour_widget.add_named_widget_shown_callback(
                ["Menu", "Normal"], self._dock_docking_normal
            )
        except ValueError:
            tour_widget.text = (
                "You may have missed a step in the tour.<br>"
                + "Please start again and carefully follow the instructions."
            )

    def _dock_docking_normal(self, widget):
        """Tell the user to choose Normal docking"""
        tour_widget = flare.tour.TourWidget()
        try:
            tour_widget.widget = flare.tour.find_widget(["Menu", "Normal"])
            tour_widget.text = "Now choose <b>Normal</b>."
            tour_widget.add_named_widget_shown_callback(
                ["Dialog", "Docking Calculation"], self._dock_docking_step1
            )
        except ValueError:
            tour_widget.text = (
                "You may have missed a step in the tour.<br>"
                + "Please start again and carefully follow the instructions."
            )

    def _dock_docking_step1(self, widget):
        """Tell the user to choose the docking method"""
        tour_widget = flare.tour.TourWidget()
        try:
            tour_widget.widget = flare.tour.find_widget(["Dialog", "Docking Calculation"])
            tour_widget.text = (
                "<b>Set-up the docking run</b>.<br>&nbsp;<br>"
                "<b>Step 1.</b> Choose the <b>Calculation method</b> from:<br>&nbsp;<br>"
                + "<b>Quick</b>: faster but slightly less precise docking "
                + "algorithm, recommended for screening "
                + "of large ligand libraries where processing speed "
                + "is very important<br>&nbsp;<br>"
                + "<b>Normal</b>: default method, optimized to provide an "
                + "accurate and exhaustive search algorithm<br>&nbsp;<br>"
                + "<b>Accurate but slow</b>: same algorithm as Normal, but "
                + "running 3 independent docking runs and keeping "
                + "the best poses overall<br>&nbsp;<br>"
                + "<b>Very Accurate but slow</b>: extra-precision docking "
                + "quality to increase accuracy and reliability of predictions. "
                + "Runs 3 independent docking runs, keeping the best poses "
                + "overall.<br>&nbsp;<br>"
                + "<b>Score Only</b>: calculates the dG and VS scores for the "
                + "ligands, allowing optimization of the poses. "
            )
            tour_widget.add_next_callback(self._dock_docking_step2)
        except ValueError:
            tour_widget.text = (
                "You may have missed a step in the tour.<br>"
                + "Please start again and carefully follow the instructions."
            )

    def _dock_docking_step2(self):
        """Tell the user to choose whether they want to use template docking."""
        tour_widget = flare.tour.TourWidget()
        try:
            tour_widget.widget = flare.tour.find_widget(["Dialog", "Docking Calculation"])
            tour_widget.text = (
                "<b>Step 2.</b> Choose whether to use a <b>Template ligand</b><br>&nbsp;<br>"
                + "When using template docking, the molecules to be "
                + "docked are aligned by substructure to the template ligand "
                + "and the aligned conformation is used to seed the docking "
                + "run, generally leading to improved docking results.<br>&nbsp;<br>"
                + "Choose a <b>Template Ligand</b> from the list to perform a "
                + "template docking experiment, or set the list to 'None' "
                + "if you don't want to use template docking."
            )
            tour_widget.add_next_callback(self._dock_docking_step3)
        except ValueError:
            tour_widget.text = (
                "You may have missed a step in the tour.<br>"
                + "Please start again and carefully follow the instructions."
            )

    def _dock_docking_step3(self):
        """Tell the user to set the energy grid"""
        tour_widget = flare.tour.TourWidget()
        try:
            tour_widget.widget = flare.tour.find_widget(["Dialog", "Docking Calculation"])
            tour_widget.text = (
                "<b>Step 3.</b> Define the <b>Energy grid</b><br>&nbsp;<br>"
                + "Set the boundaries of the search space for docking a ligand, "
                + "typically around a protein's active site.<br>&nbsp;<br>"
                + "The energy grid box can be defined by choosing from:<br>&nbsp;<br>"
                + "<b>Atom Pick</b>: ligands will be docked to the region defined "
                + "by the bounding box of the picked atoms<br>&nbsp;<br>"
                + "<b>Ligand</b>: ligands will be docked to the region defined by the chosen "
                + "ligand, expanded to include by default the protein atoms within 6 Angstroms"
                + "<br>&nbsp;<br>"
                + "<b>Existing Grid</b>: use the energy grid saved with the last docking "
                + "run for that protein."
            )
            tour_widget.add_next_callback(self._dock_docking_step4)
        except ValueError:
            tour_widget.text = (
                "You may have missed a step in the tour.<br>"
                + "Please start again and carefully follow the instructions."
            )

    def _dock_docking_step4(self):
        """Tell the user to choose the protein to dock into"""
        tour_widget = flare.tour.TourWidget()
        try:
            tour_widget.widget = flare.tour.find_widget(["Dialog", "Docking Calculation"])
            tour_widget.text = (
                "<b>Step 4:</b> Choose the <b>Protein</b><br>&nbsp;<br>"
                + "Choose the protein to use in the docking experiment from the list."
            )
            tour_widget.add_next_callback(self._dock_docking_step5)
        except ValueError:
            tour_widget.text = (
                "You may have missed a step in the tour.<br>"
                + "Please start again and carefully follow the instructions."
            )

    def _dock_docking_step5(self):
        """Tell the user to choose the protein chains"""
        tour_widget = flare.tour.TourWidget()
        try:
            tour_widget.widget = flare.tour.find_widget(["Dialog", "Docking Calculation"])
            tour_widget.text = (
                "<b>Step 4:</b> Choose the <b>protein Chains</b><br>&nbsp;<br>"
                + "Choose the protein chains you want to include in the docking "
                + "experiment by ticking/unticking them.<br>"
                + "Any chain not ticked will be ignored in the docking experiment.<br>"
            )
            tour_widget.add_next_callback(self._dock_docking_step6)
        except ValueError:
            tour_widget.text = (
                "You may have missed a step in the tour.<br>"
                + "Please start again and carefully follow the instructions."
            )

    def _dock_docking_step6(self):
        """Tell the user to choose where to save the docking results"""
        tour_widget = flare.tour.TourWidget()
        try:
            tour_widget.widget = flare.tour.find_widget(
                ["Dialog", "Docking Calculation", "Add Role"]
            )
            tour_widget.text = (
                "<b>Step 5</b>: Keep <b>Copy ligands</b> ticked to perform the docking "
                + "experiment on a copy of your ligands.<br>"
                + "You can use the drop-down menu to choose where to save the docked ligands:<br>"
                + "pressing the <b>Add Role</b> button you can create a new role for the "
                + "docked ligands."
            )
            tour_widget.add_next_callback(self._dock_docking_step7)
        except ValueError:
            tour_widget.text = (
                "You may have missed a step in the tour.<br>"
                + "Please start again and carefully follow the instructions."
            )

    def _dock_docking_step7(self):
        """Tell the user to start the docking"""
        tour_widget = flare.tour.TourWidget()
        try:
            tour_widget.widget = flare.tour.find_widget(["Dialog", "Docking Calculation", "Start"])
            tour_widget.text = (
                "<b>Step 6</b>: Press <b>Start</b> to dock your ligands, "
                + "then wait for the calculation to complete."
            )
            tour_widget.add_widget_hidden_callback(self._dock_docking_done)
        except ValueError:
            tour_widget.text = (
                "You may have missed a step in the tour.<br>"
                + "Please start again and carefully follow the instructions."
            )

    def _dock_docking_done(self):
        """Tell the user to check the poses in the Ligands table."""
        tour_widget = flare.tour.TourWidget()
        try:
            tour_widget.widget = flare.tour.find_widget("Ligands")
            tour_widget.text = (
                "On completion of the docking run, "
                + "press the small arrow (Windows, macOS) or 'plus button' "
                + "(Linux) on the left of each docked ligand "
                + "to inspect alternative poses.<br>&nbsp;<br>"
                + "Docked poses are sorted by ascending 'LF Rank Score', "
                + "a scoring function "
                + "designed to provide the best score to the correct "
                + "(experimentally observed) ligand pose.<br>"
                + "The lower (more negative) is the LF Rank Score, "
                + "the higher is the likelihood that the docked pose "
                + "reproduces the crystallographic pose.<br>&nbsp;<br>"
                + "Scroll to the right of the Ligands table to find and "
                + "inspect the LF Rank Score column. <br>&nbsp;<br>"
                + "<b>This tour is now complete.<br>"
                + "Press Stop to close this widget, or Next for more "
                + "information on alternative workflows.</b>"
            )
            tour_widget.add_next_callback(self._dock_other4)
        except ValueError:
            tour_widget.text = (
                "You may have missed a step in the tour.<br>"
                + "Please start again and carefully follow the instructions."
            )

    # Other workflows

    def _dock_other4(self):
        """Other workflows."""
        tour_widget = flare.tour.TourWidget()
        try:
            tour_widget.widget = flare.tour.find_widget("Ligands")
            tour_widget.text = (
                "Docking constraints can be added to protein atoms and "
                + "protein metals by picking the protein atoms, "
                + "pressing the Constraints button in the Ligand tab, then "
                + "selecting 'Add Docking Constraints to Protein Atom(s)'.<br>&nbsp;<br>"
                + "Existing docking constraints can be removed by choosing "
                + "'Remove Docking Constraints to Protein Atom(s)' from"
                + " the Constraints button drop-down menu.<br>&nbsp;<br>"
                + "Docking constraints can also be added or removed using the "
                + "equivalent right-click commands for picked protein atoms in the 3D window."
            )
            tour_widget.add_next_callback(self._dock_other1)
        except ValueError:
            tour_widget.text = (
                "You may have missed a step in the tour.<br>"
                + "Please start again and carefully follow the instructions."
            )

    def _dock_other1(self):
        """Other workflows."""
        tour_widget = flare.tour.TourWidget()
        try:
            tour_widget.widget = flare.tour.find_widget("Ligands")
            tour_widget.text = (
                "If your protein is already prepared, you can load it "
                + "using the 'Input protonation state'.<br>&nbsp;<br>"
                + "After you have loaded your protein, manually move "
                + "the crystallographic ligand into the Ligands table by "
                + "selecting the ligand and then going to the Protein tab and pressing "
                + "'Move Ligands/Move Ligands from Selected Proteins'.<br>&nbsp;<br>"
            )
            tour_widget.add_next_callback(self._dock_other2)
        except ValueError:
            tour_widget.text = (
                "You may have missed a step in the tour.<br>"
                + "Please start again and carefully follow the instructions."
            )

    def _dock_other2(self):
        """Other workflows."""
        tour_widget = flare.tour.TourWidget()
        try:
            tour_widget.widget = flare.tour.find_widget("Ligands")
            tour_widget.text = (
                "Pressing the 'Show Options' button in the Docking Calculation dialog "
                + "will show the advanced options controlling "
                + "the docking of your ligands.<br>&nbsp;<br>"
                + "Hover with the left mouse on top of each option for "
                + "a brief description. <br>&nbsp;<br>"
                + "You can find more information about each option in "
                + "the Flare manual."
            )
            tour_widget.add_next_callback(self._dock_other3)
        except ValueError:
            tour_widget.text = (
                "You may have missed a step in the tour.<br>"
                + "Please start again and carefully follow the instructions."
            )

    def _dock_other3(self):
        """Other workflows."""
        tour_widget = flare.tour.TourWidget()
        try:
            tour_widget.widget = flare.tour.find_widget("Ligands")
            tour_widget.text = (
                "There are other ways of loading the ligands "
                + "to dock in your Flare project.<br>&nbsp;<br>"
                + "For example, they can be loaded in 2D or 3D by "
                + "copy/paste or drag/drop (Windows only) from another "
                + "project, or from a file saved locally.<br>&nbsp;<br>"
                + "They can also be loaded in 2D by copy/paste from "
                + "a chemical drawing package.<br>&nbsp;<br>"
                + "<b>Press Stop to close this widget and leave the tour.</b>"
            )
        except ValueError:
            tour_widget.text = (
                "You may have missed a step in the tour.<br>"
                + "Please start again and carefully follow the instructions."
            )
