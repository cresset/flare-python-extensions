# -*- coding: utf-8 -*-
# Copyright (C) 2025 Cresset Biomolecular Discovery Ltd.
# Released under CC-BY-SA 4.0 (https://creativecommons.org/licenses/by-sa/4.0/).
# Originally downloaded from https://gitlab.com/cresset
from cresset import flare


class AlignLigandsTour:
    """A tour on how to align ligands to reference molecules."""

    def __init__(self):
        self._align_tour()

    def _align_tour(self):
        """Start the tour"""
        tour_widget = flare.tour.TourWidget()
        try:
            tour_widget.widget = flare.tour.find_widget("File")
            tour_widget.text = (
                "<b>Conformation hunt and align your ligands to a reference ligand."
                + "</b><br>&nbsp;<br>"
                + "Before you start, check that the Ligands table is open.<br>&nbsp;<br>"
                + '<a href="reference_ligands_loaded">'
                + "The reference and ligands to align are already in the project."
                + "</a><br></a><br>"
                + '<a href="reference_loaded">'
                + "The reference is in the project, but you have to load the ligands to align."
                + "</a><br></a><br>"
                + '<a href="none_loaded">You have to load both reference and ligands to align.</a>'
            )

            tour_widget.add_link_callback(
                "reference_ligands_loaded", self._align_referenceligandsin
            )
            tour_widget.add_link_callback("reference_loaded", self._align_referencein)
            tour_widget.add_link_callback("none_loaded", self._align_nonein)

        except ValueError:
            tour_widget.text = (
                "You may have missed a step in the tour.<br>"
                + "Please start again and carefully follow the instructions."
            )

    # Option 1: reference and ligands are already in the project

    def _align_referenceligandsin(self):
        """Ask the user whether the reference molecules must be moved into a separate role"""
        tour_widget = flare.tour.TourWidget()
        try:
            tour_widget.widget = flare.tour.find_widget("Ligand")
            tour_widget.text = (
                "For the alignment to work correctly, references must be in their own role.<br>"
                + "You can use any role for your reference, "
                + "or you can use the pre-built 'Reference' role in Flare.<br>&nbsp;<br>"
                + '<a href="ligandsinref_inrole">'
                + "My references are already in their own role"
                + "</a><br></a><br>"
                + '<a href="ligandsinref_move">'
                + "I must move my references to their own role"
                + "</a><br></a><br>"
            )

            tour_widget.add_link_callback(
                "ligandsinref_inrole", self._align_referenceligandsin_ask_constraints
            )
            tour_widget.add_link_callback(
                "ligandsinref_move", self._align_referenceligandsin_moveref
            )
        except ValueError:
            tour_widget.text = (
                "You may have missed a step in the tour.<br>"
                + "Please start again and carefully follow the instructions."
            )

    def _align_referenceligandsin_moveref(self):
        """Tell the user to go to the Ligand tab."""
        tour_widget = flare.tour.TourWidget()
        try:
            tour_widget.widget = flare.tour.find_widget("Ligand")
            tour_widget.text = "Go to the <b>Ligand tab</b>."
            tour_widget.add_named_widget_shown_callback(
                ["Ligand", "Roles"], self._align_referenceligandsin_set_role
            )
        except ValueError:
            tour_widget.text = (
                "You may have missed a step in the tour.<br>"
                + "Please start again and carefully follow the instructions."
            )

    def _align_referenceligandsin_set_role(self, widget):
        """Tell the user to move the reference molecules into a separate role"""
        tour_widget = flare.tour.TourWidget()
        try:
            tour_widget.widget = flare.tour.find_widget(["Ligand", "Roles"])
            tour_widget.text = (
                "To move your reference to the 'Reference' role, select your reference ligand "
                + "then press the <b>Roles</b> button.<br>"
            )
            tour_widget.add_named_widget_shown_callback(
                ["Menu", "Set Role"], self._align_referenceligandsin_choose_role
            )
        except ValueError:
            tour_widget.text = (
                "You may have missed a step in the tour.<br>"
                + "Please start again and carefully follow the instructions."
            )

    def _align_referenceligandsin_choose_role(self, widget):
        """Tell the user to move the reference molecules into a separate role"""
        tour_widget = flare.tour.TourWidget()
        try:
            tour_widget.widget = flare.tour.find_widget(["Menu", "Set Role"])
            tour_widget.text = (
                "Choose <b>Set Role</b>, then <b>Reference</b>.<br>&nbsp;<br>"
                + "Alternatively, you can create a new role for your reference ligand."
            )
            tour_widget.add_next_callback(self._align_referenceligandsin_ask_constraints)
        except ValueError:
            tour_widget.text = (
                "You may have missed a step in the tour.<br>"
                + "Please start again and carefully follow the instructions."
            )

    # Set optional constraints

    def _align_referenceligandsin_ask_constraints(self):
        """Ask the user whether they want to set constraints on the reference molecule"""
        tour_widget = flare.tour.TourWidget()
        try:
            tour_widget.widget = flare.tour.find_widget(["3D Pose"])
            tour_widget.text = (
                "As an optional step, before you start the alignment "
                + "you can define field or pharmacophore constraints on the reference ligands.<br>"
                + "Alignments which do not match the constraints will be penalized.<br>&nbsp;<br>"
                + '<a href="constraints">Add constraints</a></b><br>'
                + '<a href="noconstraints">Do not add constraints</a></b>'
            )
            tour_widget.add_link_callback("constraints", self._align_referenceligandsin_constraints)
            tour_widget.add_link_callback(
                "noconstraints", self._align_referenceligandsin_noconstraints
            )
        except ValueError:
            tour_widget.text = (
                "You may have missed a step in the tour.<br>"
                + "Please start again and carefully follow the instructions."
            )

    # Go ahead with constraints

    def _align_referenceligandsin_constraints(self):
        """Tell the user to go to the 3DPose tab."""
        tour_widget = flare.tour.TourWidget()
        try:
            tour_widget.widget = flare.tour.find_widget("3D Pose")
            tour_widget.text = "Go to the <b>3D Pose tab</b>."
            tour_widget.add_named_widget_shown_callback(
                ["3D Pose", "Constraints"], self._align_referenceligandsin_pressconstraints
            )
        except ValueError:
            tour_widget.text = (
                "You may have missed a step in the tour.<br>"
                + "Please start again and carefully follow the instructions."
            )

    def _align_referenceligandsin_pressconstraints(self, widget):
        """Tell the user to press the Constraints button."""
        tour_widget = flare.tour.TourWidget()
        try:
            tour_widget.widget = flare.tour.find_widget(["3D Pose", "Constraints"])
            tour_widget.text = (
                "Select your reference ligand, then press the <b>Constraints</b> button.<br>"
            )
            tour_widget.add_named_widget_shown_callback(
                ["Menu", "Set Ligand Constraints for Alignment and Virtual Screening"],
                self._align_referenceligandsin_press_button,
            )
        except ValueError:
            tour_widget.text = (
                "You may have missed a step in the tour.<br>"
                + "Please start again and carefully follow the instructions."
            )

    def _align_referenceligandsin_press_button(self, widget):
        """Tell the user to press the Set Ligand Constraints button."""
        tour_widget = flare.tour.TourWidget()
        tour_widget.widget = widget
        tour_widget.text = (
            "Press the <b>Set Ligand Constraints for Alignment "
            + "and Virtual Screening button.</b><br>"
        )
        tour_widget.add_named_widget_shown_callback(
            ["Constraint and Field Point Editor", "OK"],
            self._align_referenceligandsin_edit_constraints,
        )

    def _align_referenceligandsin_edit_constraints(self, widget):
        """Tell the user to set the constraints."""
        tour_widget = flare.tour.TourWidget()
        try:
            tour_widget.widget = flare.tour.find_widget(["Constraint and Field Point Editor", "OK"])
            tour_widget.text = (
                "Press the <b>Add</b> button to add field or pharmacophore constraints.<br>"
                + "Hover with the mouse on top of the Add button for more details.<br>&nbsp;<br>"
                + "When you are done, press <b>OK</b> to close the<br>"
                + "Constraints and Field Point Editor."
            )
            tour_widget.add_named_widget_shown_callback(
                ["Ligands"], self._align_referenceligandsin_constraints_done
            )
        except ValueError:
            tour_widget.text = (
                "You may have missed a step in the tour.<br>"
                + "Please start again and carefully follow the instructions."
            )

    def _align_referenceligandsin_constraints_done(self, widget):
        """Tell the user to select the ligands to align."""
        tour_widget = flare.tour.TourWidget()
        try:
            tour_widget.widget = flare.tour.find_widget("Ligands")
            tour_widget.text = "Select the ligands to align.<br>"
            tour_widget.add_next_callback(self._align_3dposetab)
        except ValueError:
            tour_widget.text = (
                "You may have missed a step in the tour.<br>"
                + "Please start again and carefully follow the instructions."
            )

    # Go ahead without constraints

    def _align_referenceligandsin_noconstraints(self):
        """Tell the user to select the ligands to align."""
        tour_widget = flare.tour.TourWidget()
        try:
            tour_widget.widget = flare.tour.find_widget("Ligands")
            tour_widget.text = "Select the ligands to align.<br><br>"
            tour_widget.add_next_callback(self._align_3dposetab)
        except ValueError:
            tour_widget.text = (
                "You may have missed a step in the tour.<br>"
                + "Please start again and carefully follow the instructions."
            )

    # Option 2: reference is in the project, ligands must be loaded

    def _align_referencein(self):
        """Ask the user whether the reference molecules must be moved into a separate role"""
        tour_widget = flare.tour.TourWidget()
        try:
            tour_widget.widget = flare.tour.find_widget("Ligand")
            tour_widget.text = (
                "For the alignment to work correctly, references must be in their own role.<br>"
                + "You can use any role for your reference, "
                + "or you can use the pre-built 'Reference' role in Flare.<br>&nbsp;<br>"
                + '<a href="ref_inrole">My references are already in their own role</a><br></a><br>'
                + '<a href="ref_move">I must move my references to their own role</a><br></a><br>'
            )

            tour_widget.add_link_callback("ref_inrole", self._align_referencein_ask_constraints)
            tour_widget.add_link_callback("ref_move", self._align_referencein_moveref)
        except ValueError:
            tour_widget.text = (
                "You may have missed a step in the tour.<br>"
                + "Please start again and carefully follow the instructions."
            )

    def _align_referencein_moveref(self):
        """Tell the user to go to the Ligand tab."""
        tour_widget = flare.tour.TourWidget()
        try:
            tour_widget.widget = flare.tour.find_widget("Ligand")
            tour_widget.text = "Go to the <b>Ligand tab</b>."
            tour_widget.add_named_widget_shown_callback(
                ["Ligand", "Roles"], self._align_referencein_set_role
            )
        except ValueError:
            tour_widget.text = (
                "You may have missed a step in the tour.<br>"
                + "Please start again and carefully follow the instructions."
            )

    def _align_referencein_set_role(self, widget):
        """Tell the user to move the reference molecules into a separate role"""
        tour_widget = flare.tour.TourWidget()
        try:
            tour_widget.widget = flare.tour.find_widget(["Ligand", "Roles"])
            tour_widget.text = (
                "To move your reference to the 'Reference' role, select your reference ligand "
                + "then press the <b>Roles</b> button.<br>"
            )
            tour_widget.add_named_widget_shown_callback(
                ["Menu", "Set Role"], self._align_referencein_choose_role
            )
        except ValueError:
            tour_widget.text = (
                "You may have missed a step in the tour.<br>"
                + "Please start again and carefully follow the instructions."
            )

    def _align_referencein_choose_role(self, widget):
        """Tell the user to move the reference molecules into a separate role"""
        tour_widget = flare.tour.TourWidget()
        try:
            tour_widget.widget = flare.tour.find_widget(["Menu", "Set Role"])
            tour_widget.text = (
                "Choose <b>Set Role</b>, then <b>Reference</b>.<br>&nbsp;<br>"
                + "Alternatively, you can create a new role for your reference ligand."
            )
            tour_widget.add_next_callback(self._align_referencein_ask_constraints)
        except ValueError:
            tour_widget.text = (
                "You may have missed a step in the tour.<br>"
                + "Please start again and carefully follow the instructions."
            )

    # Set optional constraints

    def _align_referencein_ask_constraints(self):
        """Ask the user whether they want to set constraints on the reference molecule"""
        tour_widget = flare.tour.TourWidget()
        try:
            tour_widget.widget = flare.tour.find_widget(["Ligand"])
            tour_widget.text = (
                "As an optional step, before you start the alignment "
                + "you can define field or pharmacophore constraints on the reference ligands.<br>"
                + "Alignments which do not match the constraints will be penalized.<br>&nbsp;<br>"
                + '<a href="refinconstraints">Add constraints</a></b><br>'
                + '<a href="refinnoconstraints">Do not add constraints</a></b>'
            )
            tour_widget.add_link_callback("refinconstraints", self._align_referencein_constraints)
            tour_widget.add_link_callback(
                "refinnoconstraints", self._align_referencein_noconstraints
            )
        except ValueError:
            tour_widget.text = (
                "You may have missed a step in the tour.<br>"
                + "Please start again and carefully follow the instructions."
            )

    # Go ahead with constraints

    def _align_referencein_constraints(self):
        """Tell the user to go to the 3DPose tab."""
        tour_widget = flare.tour.TourWidget()
        try:
            tour_widget.widget = flare.tour.find_widget("3D Pose")
            tour_widget.text = "Go to the <b>3D Pose tab</b>."
            tour_widget.add_named_widget_shown_callback(
                ["3D Pose", "Constraints"], self._align_referencein_pressconstraints
            )
        except ValueError:
            tour_widget.text = (
                "You may have missed a step in the tour.<br>"
                + "Please start again and carefully follow the instructions."
            )

    def _align_referencein_pressconstraints(self, widget):
        """Tell the user to press the Constraints button."""
        tour_widget = flare.tour.TourWidget()
        try:
            tour_widget.widget = flare.tour.find_widget(["3D Pose", "Constraints"])
            tour_widget.text = (
                "Select your reference ligand, then press the <b>Constraints</b> button.<br>"
            )
            tour_widget.add_named_widget_shown_callback(
                ["Menu", "Set Ligand Constraints for Alignment and Virtual Screening"],
                self._align_referencein_press_button,
            )
        except ValueError:
            tour_widget.text = (
                "You may have missed a step in the tour.<br>"
                + "Please start again and carefully follow the instructions."
            )

    def _align_referencein_press_button(self, widget):
        """Tell the user to press the Set Ligand Constraints button."""
        tour_widget = flare.tour.TourWidget()
        tour_widget.widget = widget
        tour_widget.text = (
            "Press the <b>Set Ligand Constraints for Alignment and "
            + "Virtual Screening button.</b><br>"
        )
        tour_widget.add_named_widget_shown_callback(
            ["Constraint and Field Point Editor", "OK"], self._align_referencein_edit_constraints
        )

    def _align_referencein_edit_constraints(self, widget):
        """Tell the user to set the constraints."""
        tour_widget = flare.tour.TourWidget()
        try:
            tour_widget.widget = flare.tour.find_widget(["Constraint and Field Point Editor", "OK"])
            tour_widget.text = (
                "Press the <b>Add</b> button to add field or pharmacophore constraints.<br>"
                + "Hover with the mouse on top of the Add button for more details.<br>&nbsp;<br>"
                + "When you are done, press <b>OK</b> to close the<br>"
                + "Constraints and Field Point Editor."
            )
            tour_widget.add_named_widget_shown_callback(
                ["File"], self._align_referencein_load_ligands
            )
        except ValueError:
            tour_widget.text = (
                "You may have missed a step in the tour.<br>"
                + "Please start again and carefully follow the instructions."
            )

    def _align_referencein_load_ligands(self, widget):
        """Tell the user to open the file menu."""
        tour_widget = flare.tour.TourWidget()
        try:
            tour_widget.widget = flare.tour.find_widget("File")
            tour_widget.text = "Press the <b>File</b> button to load the ligands to align."
            tour_widget.add_named_widget_shown_callback(
                "Open File", self._align_referencein_open_file_menu
            )
        except ValueError:
            tour_widget.text = (
                "You may have missed a step in the tour.<br>"
                + "Please start again and carefully follow the instructions."
            )

    # Go ahead without constraints

    def _align_referencein_noconstraints(self):
        """Tell the user to open the file menu."""
        tour_widget = flare.tour.TourWidget()
        try:
            tour_widget.widget = flare.tour.find_widget("File")
            tour_widget.text = "Press the <b>File</b> button to load the ligands to align."
            tour_widget.add_named_widget_shown_callback(
                "Open File", self._align_referencein_open_file_menu
            )
        except ValueError:
            tour_widget.text = (
                "You may have missed a step in the tour.<br>"
                + "Please start again and carefully follow the instructions."
            )

    def _align_referencein_open_file_menu(self, widget):
        """Tell the user to press Open File in the File menu."""
        tour_widget = flare.tour.TourWidget()
        tour_widget.widget = widget
        tour_widget.text = "Press <b>Open File</b> to load the ligands to align."
        tour_widget.add_named_widget_shown_callback(
            "Choose a file", self._align_referencein_choose_file
        )

    def _align_referencein_choose_file(self, widget):
        """Tell the user to choose a file."""
        tour_widget = flare.tour.TourWidget()
        tour_widget.widget = widget
        tour_widget.text = "Browse to find the ligands file and press <b>Open</b>."

        tour_widget.add_named_widget_shown_callback(
            "Open Molecules", self._align_referencein_ligands_open_file_options
        )
        tour_widget.add_named_widget_shown_callback(
            "CSV Import", self._align_referencein_ligands_open_csv
        )

    def _align_referencein_ligands_open_file_options(self, widget):
        """Tell the user to choose the import options"""
        tour_widget = flare.tour.TourWidget()
        tour_widget.widget = widget
        tour_widget.text = (
            "Choose the input options for the ligands being imported.<br>&nbsp;<br>"
            + "1. The Molecule type should be 'Autodetect' or 'Ligands'.<br>&nbsp;<br>"
            + "2. Choose a role for the ligands, "
            + "<b>making sure they don't end up in the same role "
            + " as the Reference</b>.<br>"
            + "If you keep 'Autodetect', the ligands will be imported "
            + " in a role named after the file you are importing.<br>&nbsp;<br>"
            + "3. <b>Choose the Protonation state:</b><br>"
            + "Choose <b>Use input protonation state</b>"
            + " if you know that your ligands are in the correct protonation state.<br>"
            + "Choose <b>Apply rule-based protonation</b> "
            + "to let Flare define the protonation state.<br>&nbsp;<br>"
            + "4. The 'Ligand file read mode option' chooses how the input files are handled. "
            + "'Autodetect' works well in most cases.<br>&nbsp;<br>"
            + "Press <b>Open</b> to import your ligands."
        )

        tour_widget.add_named_widget_shown_callback(
            "Ligands", self._align_referencein_select_ligands_toalign
        )

    def _align_referencein_ligands_open_csv(self, widget):
        """Tell the user to choose csv import options."""
        tour_widget = flare.tour.TourWidget()
        tour_widget.widget = widget
        tour_widget.text = (
            "Choose the input options for the ligands being imported.<br>&nbsp;<br>"
            + "1. Check the <b>Preview</b> panel to ensure that "
            + "the <b>Column Delimiter</b> is set correctly.<br>&nbsp;<br>"
            + "2. Choose a <b>Ligand Role</b> for the ligands, "
            + "<b>making sure they don't end up in the same role"
            + " as the Reference</b>.<br>&nbsp;<br>"
            + "3. <b>Choose the Protonation state:</b><br>"
            + "Choose <b>Use input protonation state</b> "
            + " if you know that your ligands are in the correct protonation state.<br>"
            + "Choose <b>Apply rule-based protonation</b> "
            + "to let Flare define the protonation state.<br>&nbsp;<br>"
            + "4. Choose the columns in the CSV file which map the <b>Structure</b>,"
            + " and the <b>Ligand Name</b>.<br>&nbsp;<br>"
            + "Press <b>Open</b> to import your ligands."
        )

        tour_widget.add_named_widget_shown_callback(
            "Ligands", self._align_referencein_select_ligands_toalign
        )

    def _align_referencein_select_ligands_toalign(self, widget):
        """Tell the user to select the ligands to align."""
        tour_widget = flare.tour.TourWidget()
        try:
            tour_widget.widget = flare.tour.find_widget("Ligands")
            tour_widget.text = (
                "Load additional ligands to align by selecting File/Open File again.<br>&nbsp;<br>"
                + "When you are done, select all the the ligands you want to align."
            )
            tour_widget.add_next_callback(self._align_3dposetab)
        except ValueError:
            tour_widget.text = (
                "You may have missed a step in the tour.<br>"
                + "Please start again and carefully follow the instructions."
            )

    # Option 3: reference and ligands must be loaded

    def _align_nonein(self):
        """Ask the users if they want to use a ligand or a protein for the references."""
        tour_widget = flare.tour.TourWidget()
        try:
            tour_widget.widget = flare.tour.find_widget("File")
            tour_widget.text = (
                "Choose how you want to import your reference:<br>&nbsp;<br>"
                + '<a href="file">from a ligand file</a><br>'
                + '<a href="openprotein">from a saved protein file</a><br>'
                + '<a href="downloadprotein">from a downloaded protein file.</a><br>'
            )
            tour_widget.add_link_callback("file", self._align_nonein_referencefile_filetab)
            tour_widget.add_link_callback("downloadprotein", self._align_nonein_download_filetab)
            tour_widget.add_link_callback("openprotein", self._align_nonein_filetab)
        except ValueError:
            tour_widget.text = (
                "You may have missed a step in the tour.<br>"
                + "Please start again and carefully follow the instructions."
            )

    # Import ligand as a reference

    def _align_nonein_referencefile_filetab(self):
        """Tell the user to go to the File tab"""
        tour_widget = flare.tour.TourWidget()
        try:
            tour_widget.widget = flare.tour.find_widget("File")
            tour_widget.text = (
                "Go to the <b>File</b> tab to load your reference ligand.<br>&nbsp;<br>"
                + "<b>Note</b> that the ligand must be in a "
                + "sensible 3D conformation to be used as a reference."
            )
            tour_widget.add_named_widget_shown_callback(
                "Open File", self._align_nonein_referencefile_openfile
            )
        except ValueError:
            tour_widget.text = (
                "You may have missed a step in the tour.<br>"
                + "Please start again and carefully follow the instructions."
            )

    def _align_nonein_referencefile_openfile(self, widget):
        """Tell the user to press Open File in the File menu."""
        tour_widget = flare.tour.TourWidget()
        tour_widget.widget = widget
        tour_widget.text = "Press <b>Open File</b> to load the reference ligand."
        tour_widget.add_named_widget_shown_callback(
            "Choose a file", self._align_nonein_referencefile_choose_file
        )

    def _align_nonein_referencefile_choose_file(self, widget):
        """Tell the user to choose a file."""
        tour_widget = flare.tour.TourWidget()
        tour_widget.widget = widget
        tour_widget.text = "Browse to find the reference file and press <b>Open</b>."
        tour_widget.add_named_widget_shown_callback(
            "Open Molecules", self._align_nonein_referencefile_file_options
        )
        tour_widget.add_named_widget_shown_callback(
            "CSV Import", self._align_nonein_referencefile_csv
        )

    def _align_nonein_referencefile_csv(self, widget):
        """Tell the user that they can't use a 2D reference"""
        tour_widget = flare.tour.TourWidget()
        try:
            tour_widget.widget = flare.tour.find_widget("CSV Import")
            tour_widget.text = (
                "You cannot use a 2D ligand as the reference.<br>"
                + "Please start again and choose a ligand in an appropriate 3D conformation"
                + " as the reference."
            )
        except ValueError:
            tour_widget.text = (
                "You may have missed a step in the tour.<br>"
                + "Please start again and carefully follow the instructions."
            )

    def _align_nonein_referencefile_file_options(self, widget):
        """Tell the user to choose the import options for the reference."""
        tour_widget = flare.tour.TourWidget()
        tour_widget.widget = widget
        tour_widget.text = (
            "Choose the input options for the reference ligand.<br>&nbsp;<br>"
            + "1. The Molecule type should be 'Autodetect' or 'Ligands'.<br>&nbsp;<br>"
            + "2. <b>Change the Ligand role to 'Reference'.</b><br>"
            + "If you keep 'Autodetect', the reference ligand will be imported "
            + " in a role named after the file you are importing.<br>&nbsp;<br>"
            + "3. <b>Choose the Protonation state:</b><br>"
            + "Choose <b>Input protonation state</b> "
            + " if you know that your reference is in the correct protonation state.<br>"
            + "Choose <b>Apply rule-based protonation</b> "
            + "to let Flare define the protonation state.<br>&nbsp;<br>"
            + "4. The 'Ligand file read mode' option chooses how the input files are handled. "
            + "'Autodetect' works well in most cases.<br>&nbsp;<br>"
            + "Press <b>Open</b> to import your reference."
        )

        tour_widget.add_named_widget_shown_callback(
            "Ligands", self._align_nonein_select_reference_imported
        )

    def _align_nonein_select_reference_imported(self, widget):
        """Tell the user that the reference was imported"""
        tour_widget = flare.tour.TourWidget()
        tour_widget.text = (
            "The reference ligand has been imported into the Reference role.<br>&nbsp;<br>"
            + "<b>Note:</b><br>"
            + "you can use up to 9 references to align your ligands, "
            + "however the references must be sensibly overlaid in 3D for the ligand alignment "
            + "to give meaningful results.<br>"
            + "This must be done before starting the alignment experiment.<br>"
        )
        tour_widget.add_next_callback(self._align_nonein_ask_constraints)

    # Load protein from file

    def _align_nonein_filetab(self):
        """Tell the user to go to the File tab"""
        tour_widget = flare.tour.TourWidget()
        try:
            tour_widget.widget = flare.tour.find_widget("File")
            tour_widget.text = "Go to the <b>File</b> tab to open the protein file.<br>"
            tour_widget.add_named_widget_shown_callback(
                "Open File", self._align_nonein_protein_openfile
            )
        except ValueError:
            tour_widget.text = (
                "You may have missed a step in the tour.<br>"
                + "Please start again and carefully follow the instructions."
            )

    def _align_nonein_protein_openfile(self, widget):
        """Tell the user to press the Open File button"""
        tour_widget = flare.tour.TourWidget()
        tour_widget.widget = widget
        tour_widget.text = "Press the <b>Open File</b> button."
        tour_widget.add_named_widget_shown_callback(
            "Choose a file", self._align_nonein_protein_choosefile
        )

    def _align_nonein_protein_choosefile(self, widget):
        """Tell the user find the protein file they want to use"""
        tour_widget = flare.tour.TourWidget()
        tour_widget.widget = widget
        tour_widget.text = "Browse to find the protein file, then press <b>Open</b>.<br>"
        tour_widget.add_named_widget_shown_callback(
            ["Dialog", "Open Molecules"], self._align_nonein_protein_prep
        )

    # Download protein

    def _align_nonein_download_filetab(self):
        """Tell the user to go to the File tab"""
        tour_widget = flare.tour.TourWidget()
        try:
            tour_widget.widget = flare.tour.find_widget("File")
            tour_widget.text = "Go to the <b>File</b> tab to download the protein file.<br>"
            tour_widget.add_named_widget_shown_callback(
                "Open File", self._align_nonein_download_download
            )
        except ValueError:
            tour_widget.text = (
                "You may have missed a step in the tour.<br>"
                + "Please start again and carefully follow the instructions."
            )

    def _align_nonein_download_download(self, widget):
        """Tell the user to press the Download button"""
        tour_widget = flare.tour.TourWidget()
        tour_widget.widget = widget
        tour_widget.text = "Press the <b>Download PDB</b> button."
        tour_widget.add_named_widget_shown_callback(
            "PDB Downloader", self._align_nonein_download_pdbcodes
        )

    def _align_nonein_download_pdbcodes(self, widget):
        """Tell the user type the pdb code of the protein they want to use"""
        tour_widget = flare.tour.TourWidget()
        tour_widget.widget = widget
        tour_widget.text = "Type the PDB code of the protein, then press <b>OK</b>.<br>"
        tour_widget.add_named_widget_shown_callback(
            ["Dialog", "Open Molecules"], self._align_nonein_protein_prep
        )

    def _align_nonein_protein_prep(self, widget):
        """Tell the user to prepare the protein"""
        tour_widget = flare.tour.TourWidget()
        tour_widget.widget = widget
        tour_widget.text = (
            "The Molecule type should be 'Autodetect' or 'Proteins'.<br>&nbsp;<br>"
            "Set the Protonation state to <b>Do full preparation on proteins and ligands</b>, "
            + "then press <b>Open</b>."
        )
        tour_widget.add_named_widget_shown_callback(
            ["Protein Preparation", "Add Role"], self._align_nonein_protein_prep_options_step1
        )
        tour_widget.add_named_widget_shown_callback(
            "Protein", self._align_nonein_protein_prep_not_selected
        )

    def _align_nonein_protein_prep_not_selected(self, widget):
        """If the protein prep option was not selected tell the user to do it manually."""
        tour_widget = flare.tour.TourWidget()
        tour_widget.widget = widget
        tour_widget.text = (
            "You didn't choose "
            + "<b>Do full preparation on proteins and ligands</b> "
            + "in the previous step.<br>&nbsp;<br>"
            + " Let's prepare the protein manually. Go to the <b>Protein</b> tab."
        )
        tour_widget.add_named_widget_shown_callback(
            ["Protein", "Protein Prep"], self._align_nonein_protein_protein_tab
        )

    def _align_nonein_protein_protein_tab(self, widget):
        """Tell the user to open the Protein tab."""
        tour_widget = flare.tour.TourWidget()
        tour_widget.widget = widget
        tour_widget.text = "Now press <b>Protein Prep.</b>"
        tour_widget.add_named_widget_shown_callback(
            ["Protein Preparation", "Add Role"], self._align_nonein_protein_prep_options_step1
        )

    def _align_nonein_protein_prep_options_step1(self, widget):
        """Tell the user to set the protein preparation options"""
        tour_widget = flare.tour.TourWidget()
        tour_widget.widget = widget
        tour_widget.text = (
            "Choose the protein preparation options.<br>"
            + "1. Keep the <b>Auto-Extract Ligand</b> check box <b>ticked</b>.<br>"
        )
        tour_widget.add_next_callback(self._align_nonein_protein_prep_options_step2)

    def _align_nonein_protein_prep_options_step2(self):
        """Tell the user to set the protein preparation options"""
        tour_widget = flare.tour.TourWidget()
        try:
            tour_widget.widget = flare.tour.find_widget(
                ["Dialog", "Protein Preparation", "Add Role"]
            )
            tour_widget.text = (
                "2. Change the <b>Ligand Role Name</b> to <b>Reference</b>.<br>"
                + "Press <b>Start</b> to start the protein preparation."
            )
            tour_widget.add_named_widget_shown_callback(
                ["Window", "Flare", "Dock", "Calculations", "Details"],
                self._align_nonein_protein_prep_progress,
            )
        except ValueError:
            tour_widget.text = (
                "You may have missed a step in the tour.<br>"
                + "Please start again and carefully follow the instructions."
            )

    def _align_nonein_protein_prep_progress(self, widget):
        """Wait for protein prep to run."""
        tour_widget = flare.tour.TourWidget()
        tour_widget.widget = flare.tour.find_widget(
            ["Window", "Flare", "Dock", "Calculations", "Details"]
        )
        tour_widget.text = "Wait for the protein preparation to complete."
        tour_widget.add_widget_hidden_callback(self._align_nonein_protein_prep_done)

    def _align_nonein_protein_prep_done(self):
        """Tell the user that the protein preparation is complete."""
        tour_widget = flare.tour.TourWidget()
        try:
            tour_widget.widget = flare.tour.find_widget(["Ligands"])
            tour_widget.text = (
                "The protein has now been prepared, and the ligand "
                + "moved to the Reference role.<br>&nbsp;<br>"
                + "<b>Note:</b><br>"
                + "you can use up to 9 references to align your ligands, "
                + "however the references must be sensibly overlaid "
                + "in 3D for the ligand alignment "
                + "to give meaningful results.<br>"
                + "This must be done before starting the alignment experiment, "
                + "for example by superimposing the "
                + "protein structures using the Align and Superimpose "
                + "buttons in the Protein tab.<br>"
            )
            tour_widget.add_next_callback(self._align_nonein_ask_constraints)
        except ValueError:
            tour_widget.text = (
                "You may have missed a step in the tour.<br>"
                + "Please start again and carefully follow the instructions."
            )

    # Set optional constraints

    def _align_nonein_ask_constraints(self):
        """Ask the user whether they want to set constraints on the reference molecule"""
        tour_widget = flare.tour.TourWidget()
        try:
            tour_widget.widget = flare.tour.find_widget("Ligand")
            tour_widget.text = (
                "As an optional step, before you start the alignment "
                + "you can define field or pharmacophore constraints on the reference ligands.<br>"
                + "Alignments which do not match the constraints will be penalized.<br>&nbsp;<br>"
                + '<a href="refloadconstraints">Add constraints</a><br>'
                + '<a href="refloadnoconstraints">Do not add constraints</a>'
            )
            tour_widget.add_link_callback(
                "refloadconstraints", self._align_nonein_constraints_3dposetab
            )
            tour_widget.add_link_callback(
                "refloadnoconstraints", self._align_nonein_noconstraints_filemenu
            )
        except ValueError:
            tour_widget.text = (
                "You may have missed a step in the tour.<br>"
                + "Please start again and carefully follow the instructions."
            )

    # Go ahead with constraints

    def _align_nonein_constraints_3dposetab(self):
        """Tell the user to go to the 3DPose tab."""
        tour_widget = flare.tour.TourWidget()
        try:
            tour_widget.widget = flare.tour.find_widget("3D Pose")
            tour_widget.text = "Go to the <b>3D Pose tab</b>."
            tour_widget.add_named_widget_shown_callback(
                ["3D Pose", "Constraints"], self._align_nonein_pressconstraints
            )
        except ValueError:
            tour_widget.text = (
                "You may have missed a step in the tour.<br>"
                + "Please start again and carefully follow the instructions."
            )

    #   def _align_nonein_pressconstraints(self,widget):
    #       """Tell the user to go back to the Ligand tab"""
    #      tour_widget = flare.tour.TourWidget()
    #      try:
    #           tour_widget.widget = flare.tour.find_widget("Ligand")
    #           tour_widget.text = "Go to the <b>Ligand tab<b><br>"
    #           tour_widget.add_named_widget_shown_callback(
    #               ["Ligand", "Constraints"], self._align_nonein_constr_constraints
    #           )
    #       except ValueError:
    #           tour_widget.text = (
    #               "You may have missed a step in the tour.<br>"
    #              + "Please start again and carefully follow the instructions."
    #           )

    def _align_nonein_pressconstraints(self, widget):
        """Tell the user to press the Constraints button."""
        tour_widget = flare.tour.TourWidget()
        try:
            tour_widget.widget = flare.tour.find_widget(["3D Pose", "Constraints"])
            tour_widget.text = (
                "Select your reference ligand, then press the <b>Constraints</b> button.<br>"
            )
            tour_widget.add_named_widget_shown_callback(
                ["Menu", "Set Ligand Constraints for Alignment and Virtual Screening"],
                self._align_nonein_constr_constraints_press_button,
            )
        except ValueError:
            tour_widget.text = (
                "You may have missed a step in the tour.<br>"
                + "Please start again and carefully follow the instructions."
            )

    def _align_nonein_constr_constraints_press_button(self, widget):
        """Tell the user to press the Set Ligand Constraints button."""
        tour_widget = flare.tour.TourWidget()
        tour_widget.widget = widget
        tour_widget.text = (
            "Press the <b>Set Ligand Constraints "
            + "for Alignment and Virtual Screening button.</b><br>"
        )
        tour_widget.add_named_widget_shown_callback(
            ["Constraint and Field Point Editor", "OK"], self._align_nonein_constr_edit_constraints
        )

    def _align_nonein_constr_edit_constraints(self, widget):
        """Tell the user to set the constraints."""
        tour_widget = flare.tour.TourWidget()
        try:
            tour_widget.widget = flare.tour.find_widget(["Constraint and Field Point Editor", "OK"])
            tour_widget.text = (
                "Press the <b>Add</b> button to add field or pharmacophore constraints.<br>"
                + "Hover with the mouse on top of the Add button for more details.<br>&nbsp;<br>"
                + "When you are done, press <b>OK</b> to close the<br>"
                + "Constraints and Field Point Editor."
            )
            tour_widget.add_named_widget_shown_callback(
                ["File"], self._align_nonein_constr_load_ligands
            )
        except ValueError:
            tour_widget.text = (
                "You may have missed a step in the tour.<br>"
                + "Please start again and carefully follow the instructions."
            )

    def _align_nonein_constr_load_ligands(self, widget):
        """Tell the user to open the File menu."""
        tour_widget = flare.tour.TourWidget()
        try:
            tour_widget.widget = flare.tour.find_widget("File")
            tour_widget.text = "Press the <b>File</b> button to load the ligands to align."
            tour_widget.add_named_widget_shown_callback(
                "Open File", self._align_nonein_ligands_openfile
            )
        except ValueError:
            tour_widget.text = (
                "You may have missed a step in the tour.<br>"
                + "Please start again and carefully follow the instructions."
            )

    # Go ahead without constraints, open file with ligands to align

    def _align_nonein_noconstraints_filemenu(self):
        """Tell the user to open the File menu."""
        tour_widget = flare.tour.TourWidget()
        try:
            tour_widget.widget = flare.tour.find_widget("File")
            tour_widget.text = "Press the <b>File</b> button to load the ligands to align."
            tour_widget.add_named_widget_shown_callback(
                "Open File", self._align_nonein_ligands_openfile
            )
        except ValueError:
            tour_widget.text = (
                "You may have missed a step in the tour.<br>"
                + "Please start again and carefully follow the instructions."
            )

    def _align_nonein_ligands_openfile(self, widget):
        """Tell the user to press Open File in the File menu."""
        tour_widget = flare.tour.TourWidget()
        tour_widget.widget = widget
        tour_widget.text = "Press <b>Open File</b> to load the ligands to align."
        tour_widget.add_named_widget_shown_callback(
            "Choose a file", self._align_nonein_ligands_choose_file
        )

    def _align_nonein_ligands_choose_file(self, widget):
        """Tell the user to choose a file."""
        tour_widget = flare.tour.TourWidget()
        tour_widget.widget = widget
        tour_widget.text = "Browse to find the ligand file and press <b>Open</b>."

        tour_widget.add_named_widget_shown_callback(
            "Open Molecules", self._align_nonein_ligands_open_file_options
        )
        tour_widget.add_named_widget_shown_callback(
            "CSV Import", self._align_nonein_ligands_open_csv
        )

    def _align_nonein_ligands_open_file_options(self, widget):
        """Tell the user to choose the import options"""
        tour_widget = flare.tour.TourWidget()
        tour_widget.widget = widget
        tour_widget.text = (
            "Choose the input options for the ligands being imported.<br>&nbsp;<br>"
            + "1. The Molecule type should be 'Autodetect' or 'Ligands'.<br>&nbsp;<br>"
            + "2. Choose a role for the ligands, "
            + "<b>making sure they don't end up in the same role "
            + " as the Reference</b>.<br>"
            + "If you keep 'Autodetect', the ligands will be imported "
            + " in a role named after the file you are importing.<br>&nbsp;<br>"
            + "3. <b>Choose the Protonation state:</b><br>"
            + "Choose <b>Use input protonation state</b>"
            + " if you know that your ligands are in the correct protonation state.<br>"
            + "Choose <b>Apply rule-based protonation</b> "
            + "to let Flare define the protonation state.<br>&nbsp;<br>"
            + "4. The 'Ligand file read mode option' chooses how the input files are handled. "
            + "'Autodetect' works well in most cases.<br>&nbsp;<br>"
            + "Press <b>Open</b> to import your ligands."
        )

        tour_widget.add_named_widget_shown_callback(
            "Ligands", self._align_nonein_select_ligands_toalign
        )

    def _align_nonein_ligands_open_csv(self, widget):
        """Tell the user to choose csv import options."""
        tour_widget = flare.tour.TourWidget()
        tour_widget.widget = widget
        tour_widget.text = (
            "Choose the input options for the ligands being imported.<br>&nbsp;<br>"
            + "1. Check the <b>Preview</b> panel to ensure that "
            + "the <b>Column Delimiter</b> is set correctly.<br>&nbsp;<br>"
            + "2. Choose a <b>Ligand Role</b> for the ligands, "
            + "<b>making sure they don't end up in the same role"
            + " as the Reference</b>.<br>&nbsp;<br>"
            + "3. <b>Choose the Protonation state:</b><br>"
            + "Choose <b>Use input protonation state</b> "
            + " if you know that your ligands are in the correct protonation state.<br>"
            + "Choose <b>Apply rule-based protonation</b> "
            + "to let Flare define the protonation state.<br>&nbsp;<br>"
            + "4. Choose the columns in the CSV file which map the <b>Structure</b>,"
            + " and the <b>Ligand Name</b>.<br>&nbsp;<br>"
            + "Press <b>Open</b> to import your ligands."
        )

        tour_widget.add_named_widget_shown_callback(
            "Ligands", self._align_nonein_select_ligands_toalign
        )

    def _align_nonein_select_ligands_toalign(self, widget):
        """Tell the user to select the ligands to align."""
        tour_widget = flare.tour.TourWidget()
        try:
            tour_widget.widget = flare.tour.find_widget("Ligands")
            tour_widget.text = (
                "Load additional ligands to align by selecting File/Open File again.<br>&nbsp;<br>"
                + "When you are done, select all the ligands you want to align."
            )
            tour_widget.add_next_callback(self._align_3dposetab)
        except ValueError:
            tour_widget.text = (
                "You may have missed a step in the tour.<br>"
                + "Please start again and carefully follow the instructions."
            )

    # Do the conformation hunt and alignment

    def _align_3dposetab(self):
        """Tell the user to find the Conf Hunt and Align button."""
        tour_widget = flare.tour.TourWidget()
        try:
            tour_widget.widget = flare.tour.find_widget("Conf Hunt and Align")
            tour_widget.text = "Press the <b>Conf Hunt and Align</b> button.<br>"
            tour_widget.add_named_widget_shown_callback(
                ["Dialog", "Conf Hunt and Align"], self._align_confhuntalign_step1
            )
        except ValueError:
            tour_widget.text = "Go to the <b>3D Pose tab</b>. "
            tour_widget.add_named_widget_shown_callback(
                ["3D Pose", "Conf Hunt and Align"], self._align_confhuntalign
            )

    def _align_confhuntalign(self, widget):
        """Tell the user to press the Conf Hunt and Align button."""
        tour_widget = flare.tour.TourWidget()
        try:
            tour_widget.widget = flare.tour.find_widget(["3D Pose", "Conf Hunt and Align"])
            tour_widget.text = "Press the <b>Conf Hunt and Align</b> button<br>"
            tour_widget.add_named_widget_shown_callback(
                ["Dialog", "Conf Hunt and Align", "All Ligands"], self._align_confhuntalign_step1
            )
        except ValueError:
            tour_widget.text = (
                "You may have missed a step in the tour.<br>"
                + "Please start again and carefully follow the instructions."
            )

    def _align_confhuntalign_step1(self, widget):
        """Tell the user to choose the conf hunt method."""
        tour_widget = flare.tour.TourWidget()
        try:
            tour_widget.widget = flare.tour.find_widget(
                ["Dialog", "Conf Hunt and Align", "All Ligands"]
            )
            tour_widget.text = (
                "<b>Set-up the conformation hunt and alignment run</b>.<br>&nbsp;<br>"
                "<b>Step 1.</b> Choose the <b>Conformation hunt</b> method from:<br>&nbsp;<br>"
                + "<b>No calculation</b>: don't perform the conf hunt<br>&nbsp;<br>"
                + "<b>Quick</b>: fast, only max 50 conformations kept<br>&nbsp;<br>"
                + "<b>Normal</b>: max 100 conformations: good for most molecules<br>&nbsp;<br>"
                + "<b>Accurate but slow</b>: max 200 low energy "
                + "conformations, better for larger molecules.<br>"
                + "Significant memory usage<br>&nbsp;<br>"
                + "<b>Very Accurate but slow</b>: max 1000 low energy conformations. "
                + "The best for very large molecules, but significant time and memory usage."
            )
            tour_widget.add_next_callback(self._align_confhuntalign_step2)
        except ValueError:
            tour_widget.text = (
                "You may have missed a step in the tour.<br>"
                + "Please start again and carefully follow the instructions."
            )

    def _align_confhuntalign_step2(self):
        """Tell the user to choose the alignment method."""
        tour_widget = flare.tour.TourWidget()
        try:
            tour_widget.widget = flare.tour.find_widget(
                ["Dialog", "Conf Hunt and Align", "All Ligands"]
            )
            tour_widget.text = (
                "<b>Step 2.</b> Choose the <b>Alignment</b> method from:</b><br>&nbsp;<br>"
                + "<b>No calculation</b>: don't calculate alignments<br>&nbsp;<br>"
                + "<b>Quick</b>: fast, but some alignments may be missed<br>&nbsp;<br>"
                + "<b>Normal</b>: recommended for normal use.<br>"
                + "alignment based on electrostatic and shape<br>&nbsp;<br>"
                + "<b>Substructure</b>: dominated by common substructure "
                + "to reference<br>&nbsp;<br>"
                + "<b>Score Only</b>: only appears when molecules have "
                + "conformations and at least one alignment.<br>"
                + "Similarity for the first alignment will be "
                + "recalculated without moving the molecules."
            )
            tour_widget.add_next_callback(self._align_confhuntalign_step3)
        except ValueError:
            tour_widget.text = (
                "You may have missed a step in the tour.<br>"
                + "Please start again and carefully follow the instructions."
            )

    def _align_confhuntalign_step3(self):
        """Tell the user to check that the reference role is set correctly"""
        tour_widget = flare.tour.TourWidget()
        try:
            tour_widget.widget = flare.tour.find_widget(["Dialog", "Conf Hunt and Align"])
            tour_widget.text = (
                "<b>Step 3.</b> Check that the <b>Role containing references</b> "
                + "is set correctly."
            )
            tour_widget.add_next_callback(self._align_confhuntalign_step4)
        except ValueError:
            tour_widget.text = (
                "You may have missed a step in the tour.<br>"
                + "Please start again and carefully follow the instructions."
            )

    def _align_confhuntalign_step4(self):
        """Tell the user to choose an optional protein"""
        tour_widget = flare.tour.TourWidget()
        try:
            tour_widget.widget = flare.tour.find_widget(["Dialog", "Conf Hunt and Align"])
            tour_widget.text = (
                "<b>Step 4:</b> Choose an optional protein and protein "
                + "chains to use an excluded volume.<br>"
                + "Aligments which clash with the protein will be penalized.<br>"
            )
            tour_widget.add_next_callback(self._align_confhuntalign_step5)
        except ValueError:
            tour_widget.text = (
                "You may have missed a step in the tour.<br>"
                + "Please start again and carefully follow the instructions."
            )

    def _align_confhuntalign_step5(self):
        """Tell the user to choose where to save the aligned ligands"""
        tour_widget = flare.tour.TourWidget()
        try:
            tour_widget.widget = flare.tour.find_widget(
                ["Dialog", "Conf Hunt and Align", "Add Role"]
            )
            tour_widget.text = (
                "<b>Step 5</b>: Tick <b>Copy ligands</b> to perform the alignment "
                + "experiment on a copy of your ligands.<br>"
                + "You can then use the drop-down menu to choose where to save the "
                + "aligned ligands:<br>pressing the <b>Add Role</b> button you can create a "
                + "new role for the aligned ligands."
            )
            tour_widget.add_next_callback(self._align_confhuntalign_step6)
        except ValueError:
            tour_widget.text = (
                "You may have missed a step in the tour.<br>"
                + "Please start again and carefully follow the instructions."
            )

    def _align_confhuntalign_step6(self):
        """Tell the users to start the alignment"""
        tour_widget = flare.tour.TourWidget()
        try:
            tour_widget.widget = flare.tour.find_widget(["Dialog", "Conf Hunt and Align", "Start"])
            tour_widget.text = (
                "<b>Step 6.</b> Press the <b> Start</b> button to align your ligands, "
                + "then wait for the calculation to complete."
            )
            tour_widget.add_widget_hidden_callback(self._align_confhuntalign_alignment_done)
        except ValueError:
            tour_widget.text = (
                "You may have missed a step in the tour.<br>"
                + "Please start again and carefully follow the instructions."
            )

    def _align_confhuntalign_alignment_done(self):
        """Tell the user to check the alignments in the Ligands table."""
        tour_widget = flare.tour.TourWidget()
        try:
            tour_widget.widget = flare.tour.find_widget("Ligands")
            tour_widget.text = (
                "When the alignment is complete, "
                + "press the small arrow (Windows, macOS) or 'plus button' "
                + "(Linux) on the left of each aligned ligand "
                + "to inspect alternative alignments.<br>&nbsp;<br>"
                + "Scroll to the right of the Ligands table to find the Sim column"
                + " and view the electrostatic and shape similarity value "
                + "for each ligand.<br>&nbsp;<br>"
                + "<b>This tour is now complete.<br>"
                + "Press Stop to close this widget, or Next for more "
                + "information on alternative workflows.</b>"
            )
            tour_widget.add_next_callback(self._align_other1)
        except ValueError:
            tour_widget.text = (
                "You may have missed a step in the tour.<br>"
                + "Please start again and carefully follow the instructions."
            )

    def _align_other1(self):
        """Other workflows."""
        tour_widget = flare.tour.TourWidget()
        try:
            tour_widget.widget = flare.tour.find_widget("Ligands")
            tour_widget.text = (
                "There are other ways of loading reference and "
                + "ligands to align in your Flare project.<br>&nbsp;<br>"
                + "For example, they can be loaded by copy/paste or "
                + "drag/drop (Windows only) from another "
                + "project, or from a file saved locally.<br>&nbsp;<br>"
                + "The ligands to align can also be loaded in 2D by "
                + "copy/paste from a chemical drawing package."
            )
            tour_widget.add_next_callback(self._align_other2)
        except ValueError:
            tour_widget.text = (
                "You may have missed a step in the tour.<br>"
                + "Please start again and carefully follow the instructions."
            )

    def _align_other2(self):
        """Other workflows."""
        tour_widget = flare.tour.TourWidget()
        try:
            tour_widget.widget = flare.tour.find_widget("Ligands")
            tour_widget.text = (
                "Pressing the <b>Show Options</b> button in the Conf Hunt and Align dialog "
                + "will show the advanced options controlling the conformation "
                + "hunt and alignment of your ligands.<br>&nbsp;<br>"
                + "Hover with the left mouse on top of each option for "
                + "a brief description. <br>&nbsp;<br>"
                + "You can find more information about each option in "
                + "the Flare manual."
            )
            tour_widget.add_next_callback(self._align_other3)
        except ValueError:
            tour_widget.text = (
                "You may have missed a step in the tour.<br>"
                + "Please start again and carefully follow the instructions."
            )

    def _align_other3(self):
        """Other workflows."""
        tour_widget = flare.tour.TourWidget()
        try:
            tour_widget.widget = flare.tour.find_widget("Ligands")
            tour_widget.text = (
                "If your protein is already prepared, you can load it using "
                + "the 'Input protonation state'.<br>&nbsp;<br>"
                + "To move the ligand into the Ligands table, to be used as "
                + "a reference, select the ligand, "
                + "go to the Protein tab and press 'Move Ligands/Move Ligands "
                + "from Selected Proteins'.<br>&nbsp;<br>"
                + "<b>Press Stop to close this widget and leave the tour.</b>"
            )

        except ValueError:
            tour_widget.text = (
                "You may have missed a step in the tour.<br>"
                + "Please start again and carefully follow the instructions."
            )
