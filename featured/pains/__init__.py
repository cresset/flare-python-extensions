# -*- coding: utf-8 -*-
# Copyright (C) 2025 Cresset Biomolecular Discovery Ltd.
# Released under CC-BY-SA 4.0 (https://creativecommons.org/licenses/by-sa/4.0/).
# Originally downloaded from https://gitlab.com/cresset
"""Run the PAINS filters on a set of ligands.

Ribbon Controls:
    Ligand -> Table -> PAINS Filter
        Add a column to the ligand table which shows if each ligand passes
        or fails PAINS (Pan Assay INterference) filters.
"""

import os

from PySide2 import QtWidgets, QtCore, QtGui

from cresset import flare

from rdkit.Chem import FilterCatalog


class DialogWithOpenCallback(QtWidgets.QDialog):
    dialog_opened = QtCore.Signal()

    def __init__(self, parent_widget):
        super().__init__(parent_widget)

    # override open to provide dialog open signal
    def showEvent(self, event):
        self.dialog_opened.emit()
        super().showEvent(event)


@flare.extension
class PainExtension:
    """Add a button to the ribbon which shows if each ligand passes the PAINS filters."""

    def __init__(self):
        self._parent_widget = flare.main_window().widget()
        self._dialog = DialogWithOpenCallback(self._parent_widget)
        self._dialog.dialog_opened.connect(self._update_ui)

        self._button_box = QtWidgets.QDialogButtonBox(
            QtWidgets.QDialogButtonBox.Ok | QtWidgets.QDialogButtonBox.Cancel, self._dialog
        )

        self._button_box.accepted.connect(self._dialog.accept)
        self._button_box.rejected.connect(self._dialog.reject)

        # Create PAINS checkboxes
        self._pain_filters_checkboxes = QtWidgets.QButtonGroup()
        self._pain_filters_checkboxes.setExclusive(False)

        for pain_filter in self._available_pain_filters():
            name = pain_filter.name.replace("_", " ")
            checkbox = QtWidgets.QCheckBox(name, self._dialog)
            checkbox.setChecked(True)
            self._pain_filters_checkboxes.addButton(checkbox, int(pain_filter))
            checkbox.toggled.connect(self._update_ui)

        message_label = QtWidgets.QLabel(
            "Select the PAINS filters to apply to the molecules. "
            + "A column will be added which shows if a ligand passed all the PAINS filters. "
            + "The column tooltip for ligands which fail a PAINS "
            + "filter will show the reason for the failure.",
            self._dialog,
        )
        message_label.setWordWrap(True)

        # Select which ligands to act on
        self._all_ligands_button = QtWidgets.QRadioButton("All Ligands", self._dialog)
        self._all_ligands_button.setChecked(True)
        self._all_ligands_button.toggled.connect(self._update_ui)
        self._selected_ligands_button = QtWidgets.QRadioButton("Selected Ligands", self._dialog)
        self._selected_ligands_button.toggled.connect(self._update_ui)

        # The column to create or replace
        column_name_label = QtWidgets.QLabel("Column Name:", self._dialog)
        column_name_label.setToolTip("The name of the column to place the results in.")

        self._column_name = QtWidgets.QLineEdit(self._dialog)
        self._column_name.setText("PAINS")
        self._column_name.textChanged.connect(self._update_ui)

        # Layout the PAINS filter checkboxes
        pain_layout = QtWidgets.QGridLayout()
        pain_layout.setContentsMargins(0, 0, 0, 0)
        row_count = int((len(self._pain_filters_checkboxes.buttons()) + 0.5) / 2)
        for index, checkbox in enumerate(self._pain_filters_checkboxes.buttons()):
            column = int((index + 0.5) // row_count)
            row = int((index + 0.5) % row_count)
            pain_layout.addWidget(checkbox, row, column)

        # Layout the column name widgets
        column_name_layout = QtWidgets.QHBoxLayout()
        column_name_layout.setContentsMargins(0, 0, 0, 0)
        column_name_layout.addWidget(column_name_label)
        column_name_layout.addWidget(self._column_name, 1)

        # External link
        link_label = QtWidgets.QLabel(
            '<html><a href="https://github.com/rdkit/rdkit/pull/536">About PAINS filter</a></html>'
        )
        link_label.setOpenExternalLinks(True)

        # Layout the dialog
        layout = QtWidgets.QVBoxLayout(self._dialog)
        layout.addWidget(message_label)
        layout.addLayout(pain_layout)
        layout.addWidget(self._all_ligands_button)
        layout.addWidget(self._selected_ligands_button)
        layout.addLayout(column_name_layout)
        layout.addWidget(link_label)
        layout.addStretch(1)
        layout.addWidget(self._button_box)

        flare.callbacks.selected_ligands_changed.add(self._project_selected_ligands_changed)
        self._dialog.accepted.connect(self._on_dialog_accepted)
        self._update_ui()

    def load(self):
        """Load the extension."""
        group = flare.main_window().ribbon["Ligand"]["Table"]

        control = group.add_button("PAINS Filter", self._show_dialog)
        control.set_priority(flare.UIRibbonControl.Priority.High)
        control.tooltip = (
            "Add a column to the ligand table which shows if each ligand passes "
            + "or fails PAINS (Pan Assay INterference) filters."
        )
        control.load_icon(os.path.join(os.path.dirname(os.path.realpath(__file__)), "pains.png"))
        control.tip_key = "K"

        print(f"Loaded {self.__class__.__module__}.{self.__class__.__name__}")

    def _update_ui(self):
        """Enable/disable the ok button and updated the selected ligand count."""
        valid = (
            len(self._column_name.text().strip()) > 0
            and len(self._selected_pain_filters()) > 0
            and len(self._selected_ligands()) > 0
        )
        self._button_box.button(QtWidgets.QDialogButtonBox.Ok).setEnabled(valid)

        count_selected = len(flare.main_window().selected_ligands)
        self._selected_ligands_button.setText(
            "Selected Ligand" if count_selected == 1 else f"Selected {count_selected} Ligands"
        )

    def _available_pain_filters(self):
        """Return a list of PAINS filters."""
        FilterCatalogs = FilterCatalog.FilterCatalogParams.FilterCatalogs
        filters = []
        filters.append(FilterCatalogs.PAINS_A)
        filters.append(FilterCatalogs.PAINS_B)
        filters.append(FilterCatalogs.PAINS_C)
        filters.append(FilterCatalogs.BRENK)
        filters.append(FilterCatalogs.NIH)
        filters.append(FilterCatalogs.ZINC)
        return filters

    def _selected_pain_filters(self):
        """Return a list of PAINS filters which are checked in the dialog."""
        FilterCatalogs = FilterCatalog.FilterCatalogParams.FilterCatalogs
        pains = []
        for checkbox in self._pain_filters_checkboxes.buttons():
            if checkbox.isChecked():
                pain_id = self._pain_filters_checkboxes.id(checkbox)
                pains.append(FilterCatalogs(pain_id))
        return pains

    def _selected_ligands(self):
        """Return a list of ligands to apply the pain filters to."""
        ligands = []
        if self._all_ligands_button.isChecked():
            project = flare.main_window().project
            ligands = project.ligands
        else:
            ligands = flare.main_window().selected_ligands
        return ligands

    def _project_selected_ligands_changed(self, selected_ligands):
        """Update the UI when the selected ligands changes."""
        if selected_ligands:
            self._selected_ligands_button.setChecked(True)
        else:
            self._all_ligands_button.setChecked(True)
        self._update_ui()

    def _show_dialog(self):
        """Show the dialog.

        The dialog is non model to allow the ligand selection to be changed.
        """
        self._dialog.show()

    def _on_dialog_accepted(self):
        """Calculate pain values based on the dialog configuration."""
        pain_filters = self._selected_pain_filters()
        ligands = self._selected_ligands()
        column_name = self._column_name.text().strip()
        QtGui.QGuiApplication.setOverrideCursor(QtCore.Qt.WaitCursor)
        self._calculate_pain(ligands, pain_filters, column_name)
        QtGui.QGuiApplication.restoreOverrideCursor()

    def _calculate_pain(self, ligands, pain_filters, column_name):
        """Calculate pain values for the given ligands."""
        warn_if_tooltip_not_edited = False

        params = FilterCatalog.FilterCatalogParams()
        for pain_filter in pain_filters:
            params.AddCatalog(pain_filter)
        catalog = FilterCatalog.FilterCatalog(params)

        for ligand in ligands:
            rdmol = ligand.to_rdmol()
            result = "Pass"
            tooltip = ""

            # Do any of the PAINS filters match the ligand
            if catalog.HasMatch(rdmol):
                result = "Fail"

                # Show the reason for the fail in the tooltip
                # Check each pain filter which failed
                for entry in catalog.GetMatches(rdmol):
                    scope = entry.GetProp("Scope")
                    description = entry.GetDescription()
                    tooltip = tooltip + f"{scope}: {description}\n"

                    # A ligand may fail a PAINS filter multiple times
                    # Show what atoms caused it to fail
                    # Some filters can have 100's sets of atoms that fail so the
                    # tooltip is limited to just a few
                    max_filter_matches = 4
                    filterMatchs = entry.GetFilterMatches(rdmol)
                    for index, filterMatch in enumerate(filterMatchs):
                        if index < max_filter_matches:
                            # Flare GUI uses 1 based atom indexes
                            atom_indexes = [
                                str(atomIdx + 1) for queryAtomIdx, atomIdx in filterMatch.atomPairs
                            ]
                            if atom_indexes:
                                atom_indexes_str = ", ".join(atom_indexes)
                                tooltip = tooltip + f"    Atoms: {atom_indexes_str}\n"

                        elif index == max_filter_matches:
                            count = len(filterMatchs) - max_filter_matches
                            tooltip = tooltip + f"    ... plus {count} additional sets of atoms\n"

            # Set the ligand column value and tooltip
            try:
                ligand.properties[column_name].value = result
            except Exception:
                # Errors stating values cannot be edited and early return
                QtWidgets.QMessageBox.critical(
                    self._parent_widget,
                    "PAINS error",
                    f"Cannot change values of column '{column_name}'.",
                )
                return

            # All ligands on the same column so if it can't change for
            # one ligand, it can't change for all.
            if not warn_if_tooltip_not_edited:
                try:
                    ligand.properties[column_name].tooltip = tooltip
                except Exception:
                    warn_if_tooltip_not_edited = True

        if warn_if_tooltip_not_edited:
            QtWidgets.QMessageBox.warning(
                self._parent_widget,
                "PAINS warning",
                f"Cannot change tooltip of column '{column_name}', but values are changed.",
            )
        flare.main_window().create_undo_point(
            f"PAINS Filter on {len(ligands)} " + ("ligand" if len(ligands) == 1 else "ligands")
        )
