#!/usr/bin/env pyflare
# -*- coding: utf-8 -*-
# Copyright (C) 2025 Cresset Biomolecular Discovery Ltd.
# Released under CC-BY-SA 4.0 (https://creativecommons.org/licenses/by-sa/4.0/).
# Originally downloaded from https://gitlab.com/cresset
"""Gradient Boosting Regression QSAR model extension"""
from PySide2 import QtWidgets


def model_name():
    """Returns the name of this model which allows Flare to detect the QSAR
    model type. The name must contain either "Regression" or "Classification".
    """
    return "Gradient Boosting Regression"


def include_in_automatic():
    """Returns true if the model should be included when "Automatic" is selected
    as the model type in Flare.
    """
    return True


def standardize_descriptors():
    """Returns true if descriptor values should be standardized before creating
    the model.
    """
    return True


def print_parameters():
    """Prints the values of the hyperparameters that Flare will parse and use
    to configure the model.
    """
    from cresset import flare

    qsarwidget = flare.main_window().qsar_widget(model_name())

    modeCB = qsarwidget.findChild(QtWidgets.QComboBox, "modeCB")
    if modeCB is None:
        raise RuntimeError("Failed to find mode widget")
    print("mode=", modeCB.currentText())

    kfoldsSB = qsarwidget.findChild(QtWidgets.QSpinBox, "kfoldsSB")
    if kfoldsSB is None:
        raise RuntimeError("Failed to find kfoldsSB widget")
    print("kfolds=", kfoldsSB.value())


def restore_parameters(parameters):
    """Restores parameter values when a saved config is loaded in Flare."""
    from cresset import flare

    qsarwidget = flare.main_window().qsar_widget(model_name())

    if "mode" not in parameters:
        raise ValueError("parameters dict missing 'mode' parameter")
    if "kfolds" not in parameters:
        raise ValueError("parameters dict missing 'kfolds' parameter")

    modeCB = qsarwidget.findChild(QtWidgets.QComboBox, "modeCB")
    if modeCB is None:
        raise RuntimeError("Failed to find modeCB widget")
    mode = parameters["mode"]
    for i in range(0, modeCB.count()):
        if modeCB.itemText(i).lower() == mode:
            modeCB.setCurrentIndex(i)
            break

    kfoldsSB = qsarwidget.findChild(QtWidgets.QSpinBox, "kfoldsSB")
    if kfoldsSB is None:
        raise RuntimeError("Failed to find kfoldsSB widget")
    kfoldsSB.setValue(parameters["kfolds"])


GB_NAME = "Gradient Boosting"
HGB_NAME = "Histogram Gradient Boosting"


class ParametersWidget(QtWidgets.QWidget):
    """The widget used to configure hyperparameters for this model.
    This class is required as it allows Flare to import the widget in to the
    QSAR model building dialog on start-up.
    """

    def __init__(self):
        super().__init__()

        self._mode = QtWidgets.QComboBox(self)
        self._mode.setObjectName("modeCB")
        self._mode.setToolTip(
            """<span>Choose the type of Gradient Boosting to use. 'Auto' will use
             '{0}' for training sets smaller than 1500 and '{1}' otherwise.</span>""".format(
                GB_NAME, HGB_NAME
            )
        )
        self._mode.addItems({"Auto", GB_NAME, HGB_NAME})
        self._mode.setCurrentText("Auto")

        self._kfolds = QtWidgets.QSpinBox(self)
        self._kfolds.setObjectName("kfoldsSB")
        self._kfolds.setToolTip(
            "Set the number of folds to use in k-fold cross-validation. For example, with k=5,\n"
            "the cross-validation process will build 5 models, each time leaving out 20% of the\n"
            "data set."
        )
        self._kfolds.setRange(2, 1e6)
        self._kfolds.setValue(5)

        self._layout = QtWidgets.QFormLayout(self)
        self._layout.addRow("Mode", self._mode)
        self._layout.addRow("Number of folds in k-fold cross-validation", self._kfolds)


def create_model(x_train, y_train, mode, k_folds):
    """Create the model and return the best estimator"""
    from sklearn.model_selection import GridSearchCV

    # Check for 'Auto' here as well to support consensus model
    if mode == "Auto":
        mode = GB_NAME if len(x_train) < 1500 else HGB_NAME

    if mode == GB_NAME:
        from sklearn.ensemble import GradientBoostingRegressor

        params = {
            "n_estimators": [200, 500, 1000],
            "learning_rate": [0.01, 0.03, 0.05, 0.1, 0.2, 0.3],
            "subsample": [0.8, 0.9],
            "loss": ["absolute_error"],
            "max_depth": [6],
            "max_leaf_nodes": [64],
            "random_state": [0],
            "min_samples_leaf": [1],
        }

        model = GradientBoostingRegressor(random_state=0)
        model.fit(x_train, y_train)

    elif mode == HGB_NAME:
        from sklearn.ensemble import HistGradientBoostingRegressor

        params = {
            "max_bins": [100, 125, 175, 200, 255],
            "learning_rate": [0.005, 0.01, 0.03, 0.05, 0.1, 0.2, 0.3],
        }
        model = HistGradientBoostingRegressor(random_state=0)
        model.fit(x_train, y_train)

    grid_regressor = GridSearchCV(
        model, param_grid=params, scoring="r2", n_jobs=-1, cv=k_folds, verbose=5
    )
    grid_regressor.fit(x_train, y_train)

    return grid_regressor.best_estimator_


def main():
    """Create the model using sklearn and print the results of validation"""
    # Local imports are used to reduce the extension load time as these imports
    # are only needed when building the model
    import argparse
    import numpy as np

    import scipy.stats as stats

    from sklearn.model_selection import cross_val_predict

    from sklearn.utils import shuffle
    from sklearn.metrics import r2_score
    from sklearn.metrics import mean_squared_error

    from sklearn.decomposition import PCA

    from skl2onnx import convert_sklearn
    from skl2onnx.common.data_types import FloatTensorType

    parser = argparse.ArgumentParser(
        description="Gradient Boosting Regression Machine Learning Model"
    )
    parser.add_argument(
        "--mode",
        type=ascii,
        default="Auto",
        help="The type of model to use. 'Auto' will use '{0}' for training "
        "sets smaller than 1500 and '{1}' otherwise. (default: 'Auto')".format(GB_NAME, HGB_NAME),
    )
    parser.add_argument(
        "--kfolds",
        type=int,
        default=5,
        help="The number of folds to use in cross-validation (default: 5)",
    )
    args = parser.parse_args()

    training = np.loadtxt(open("Training.csv"), delimiter=",")

    X_train = training[:, 1:]
    y_train = training[:, 0]

    X_test = np.empty(0)
    y_test = np.empty(0)
    try:
        test = np.loadtxt(open("Test.csv"), delimiter=",")
        X_test = test[:, 1:]
        y_test = test[:, 0]
    except FileNotFoundError:
        pass

    mode = args.mode.strip("'")
    if mode == "Auto":
        mode = GB_NAME if len(X_train) < 1500 else HGB_NAME

    # Do principle component analysis before shuffling
    pca = PCA(n_components=min(10, min(X_train.shape)), random_state=0)
    principalComponents = pca.fit_transform(X_train)

    # Add a row to the end containing the explained variance ratios
    pcaOut = np.append(principalComponents, [pca.explained_variance_ratio_], 0)

    # Flare will read PCA.csv to populate the PCA plot
    np.savetxt("PCA.csv", pcaOut, delimiter=",")

    X_train, y_train = shuffle(X_train, y_train, random_state=0)
    model = create_model(X_train, y_train, mode, args.kfolds)

    train_y_predicted = model.predict(X_train)
    train_r2 = r2_score(y_train, train_y_predicted)
    train_rmse = mean_squared_error(y_train, train_y_predicted, squared=False)
    train_tau, p_value = stats.kendalltau(y_train, train_y_predicted)

    cv_predicted = cross_val_predict(model, X_train, y_train, cv=args.kfolds, n_jobs=-1)
    cv_r2 = r2_score(y_train, cv_predicted)
    cv_rmse = mean_squared_error(y_train, cv_predicted, squared=False)
    cv_tau, p_value = stats.kendalltau(y_train, cv_predicted)

    if len(X_test) > 0:
        test_y_predicted = model.predict(X_test)
        test_r2 = r2_score(y_test, test_y_predicted)
        test_rmse = mean_squared_error(y_test, test_y_predicted, squared=False)
        test_tau, p_value = stats.kendalltau(y_test, test_y_predicted)

    best_params = model.get_params()

    # Print log text
    print("")
    print("Gradient Boosting Regression model parameters:")
    print("Mode:                  ", mode)
    print("Number of k-folds:     ", args.kfolds)
    if mode == GB_NAME:
        print("N estimators:          ", best_params["n_estimators"])
        print("Learning rate:         ", best_params["learning_rate"])
        print("Subsample:             ", best_params["subsample"])
        print("Loss:                  ", best_params["loss"])
        print("Max depth:             ", best_params["max_depth"])
        print("Max leaf nodes:        ", best_params["max_leaf_nodes"])
        print("Random state:          ", best_params["random_state"])
        print("Min sample leaf:       ", best_params["min_samples_leaf"])
    elif mode == HGB_NAME:
        print("Max bins:              ", best_params["max_bins"])
        print("Learning rate:         ", best_params["learning_rate"])
    print("")

    print("Statistics for predictions from full model on", len(X_train), "compounds:")
    print("R^2:                  {:0.3f}".format(train_r2))
    print("RMSE:                 {:0.3f}".format(train_rmse))
    print("Kendall's tau:        {:0.3f}".format(train_tau))
    print("")
    print("Statistics for predictions from cross-validation on", len(X_train), "compounds:")
    print("Q^2:                  {:0.3f}".format(cv_r2))
    print("RMSE:                 {:0.3f}".format(cv_rmse))
    print("Kendall's tau:        {:0.3f}".format(cv_tau))
    print("")
    if len(X_test) > 0:
        print("Statistics for test set predictions from full model on", len(X_test), "compounds:")
        print("R^2:                  {:0.3f}".format(test_r2))
        print("RMSE:                 {:0.3f}".format(test_rmse))
        print("Kendall's tau:        {:0.3f}".format(test_tau))

    # Convert into ONNX format
    initial_type = [("float_input", FloatTensorType([None, X_train.shape[1]]))]
    onnx = convert_sklearn(model, initial_types=initial_type)
    with open("model.onnx", "wb") as f:
        f.write(onnx.SerializeToString())

    # Fill an array with indexes then shuffle them with the same seed as was used
    # for the training molecules to get them in the same order
    indexes = shuffle([i for i in range(0, X_train.shape[0])], random_state=0)

    predCV = np.empty([X_train.shape[0], 2])
    predCV[:, 0] = indexes
    predCV[:, 1] = cv_predicted

    # Write out the CV scores and molecule indexes so they can be plotted in Flare
    np.savetxt("CV.csv", predCV, delimiter=",")


if __name__ == "__main__":
    main()
