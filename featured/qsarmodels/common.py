# -*- coding: utf-8 -*-
# Copyright (C) 2025 Cresset Biomolecular Discovery Ltd.
# Released under CC-BY-SA 4.0 (https://creativecommons.org/licenses/by-sa/4.0/).
# Originally downloaded from https://gitlab.com/cresset


def print_classification_scores(conf_matrix):
    import math
    import numpy as np

    print("Confusion matrix:")
    print("\t\tPredicted")

    n_classes = len(conf_matrix[0])

    classRow = "\t"
    for col in range(0, n_classes):
        classRow += "\t{}".format(col + 1)
    print(classRow)

    tp = [0] * n_classes
    fn = [0] * n_classes
    tn = [0] * n_classes
    fp = [0] * n_classes
    total = 0
    for x in range(0, n_classes):
        columns = ""
        for y in range(0, n_classes):
            columns += "\t{}".format(conf_matrix[x][y])

            val = conf_matrix[x][y]
            total += val
            if x == y:
                tp[x] = val
            if x != y:
                fn[x] += val
                fp[y] += val
        if x == 0:
            print("Actual\t", x + 1, columns)
        else:
            print("\t", x + 1, columns)
    print("")

    class_log = []
    mean_precision = valid_precision = mean_recall = valid_recall = informedness = 0
    for x in range(0, n_classes):
        tn[x] = total - tp[x] - fp[x] - fn[x]

        precision = tp[x] / (tp[x] + fp[x])
        recall = tp[x] / (tp[x] + fn[x])
        youdens = tp[x] / (tp[x] + fn[x]) + tn[x] / (tn[x] + fp[x]) - 1

        class_log.append(
            "Class {}\t{:0.3f}\t{:0.3f}\t{:0.3f}".format(x + 1, precision, recall, youdens)
        )

        if not np.isnan(precision):
            mean_precision += precision
            valid_precision += 1
        if not np.isnan(recall):
            mean_recall += recall
            valid_recall += 1

        sum = 0
        for y in range(0, n_classes):
            ppr = conf_matrix[y][x] / total
            pr = (fn[x] + tp[x]) / total
            w = 1 / pr if (x == y) else -1 / (1 - pr)
            sum += ppr * w
        ppl = (fp[x] + tp[x]) / total
        informedness += ppl * sum

    mean_precision = mean_precision / valid_precision
    mean_recall = mean_recall / valid_recall

    f1 = 2 * (mean_precision * mean_recall) / (mean_precision + mean_recall)

    informedness_error = " (No class members)" if math.isnan(informedness) else ""

    print("Informedness (Bookmaker's):      {:0.3f}{}".format(informedness, informedness_error))
    print("F1 statistic:                    {:0.3f}".format(f1))
    print("Mean precision:                  {:0.3f}".format(mean_precision))
    print("Mean recall:                     {:0.3f}".format(mean_recall))
    print("")

    print("\tPrecision\tRecall\tYouden's J")
    for log in class_log:
        print(log)
