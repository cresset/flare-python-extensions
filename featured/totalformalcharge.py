# -*- coding: utf-8 -*-
# Copyright (C) 2025 Cresset Biomolecular Discovery Ltd.
# Released under CC-BY-SA 4.0 (https://creativecommons.org/licenses/by-sa/4.0/).
# Originally downloaded from https://gitlab.com/cresset
"""Adds a column to the ligand table which displays the ligand total formal charge."""

from cresset import flare


@flare.extension
class TotalFormalChargeExtension:
    """Calculate the total formal charge of ligands in the project."""

    def load(self):
        """Start listening to the ligands."""
        flare.callbacks.ligands_added.add(self._on_ligands_added)
        flare.callbacks.ligand_structure_changed.add(self._on_ligand_structure_changed)
        flare.callbacks.poses_added.add(self._on_poses_added)
        flare.callbacks.main_window_project_changed.add(self._on_main_window_project_changed)

        print(f"Loaded {self.__class__.__module__}.{self.__class__.__name__}")

    def _on_ligands_added(self, ligands):
        for ligand in ligands:
            self._update_ligand_total_charge(ligand)

    def _on_ligand_structure_changed(self, ligand):
        self._update_ligand_total_charge(ligand)

    def _on_poses_added(self, poses):
        ligands = {pose.ligand for pose in poses}
        for ligand in ligands:
            self._update_ligand_total_charge(ligand)

    def _on_main_window_project_changed(self, project):
        for ligand in project.ligands:
            self._update_ligand_total_charge(ligand)

    @classmethod
    def _update_ligand_total_charge(cls, ligand):
        """Add or update the "Total Formal Charge" with the ligand total formal charge."""
        total_formal_charge = ligand.total_formal_charge()
        prop = ligand.properties["Total Formal Charge"]
        prop.value = f"{total_formal_charge}"
